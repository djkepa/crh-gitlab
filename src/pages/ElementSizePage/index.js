import React from 'react';
import HookDoc from '../../components/hookdoc';
import ElementSizeComponent from '../../examples/useElementSize.example';
import ElementSizeData from '../../data/element-size.json';

function ElementSizePage() {
  return (
    <div>
      <HookDoc data={ElementSizeData}>
        <ElementSizeComponent />
      </HookDoc>
    </div>
  );
}

export default ElementSizePage;
