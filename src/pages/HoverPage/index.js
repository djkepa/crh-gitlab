import React from 'react';
import HookDoc from '../../components/hookdoc';
import HoverComponent from '../../examples/useHover.example';
import HoverData from '../../data/hover.json';

function HoverPage() {
  return (
    <div>
      <HookDoc data={HoverData}>
        <HoverComponent />
      </HookDoc>
    </div>
  );
}

export default HoverPage;
