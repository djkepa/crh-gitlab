import React from 'react';
import HookDoc from '../../components/hookdoc';
import DragDropComponent from '../../examples/useDragDrop.example';
import DragDropData from '../../data/drag-drop.json';

function DragDropPage() {
  return (
    <div>
      <HookDoc data={DragDropData}>
        <DragDropComponent />
      </HookDoc>
    </div>
  );
}

export default DragDropPage;
