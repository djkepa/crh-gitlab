import React from 'react';
import HookDoc from '../../components/hookdoc';
import ClickOutsideComponent from '../../examples/useClickOutside.example';
import clickOutsideData from '../../data/click-outside.json';

function ClickOutsidePage() {
  return (
    <div>
      <HookDoc data={clickOutsideData}>
        <ClickOutsideComponent />
      </HookDoc>
    </div>
  );
}

export default ClickOutsidePage;
