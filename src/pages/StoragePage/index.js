import React from 'react';
import HookDoc from '../../components/hookdoc';
import StorageComponent from '../../examples/useStorage.example';
import StorageData from '../../data/storage.json';

function StoragePage() {
  return (
    <div>
      <HookDoc data={StorageData}>
        <StorageComponent />
      </HookDoc>
    </div>
  );
}

export default StoragePage;
