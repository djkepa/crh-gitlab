import React from 'react';
import HookDoc from '../../components/hookdoc';
import ToggleComponent from '../../examples/useToggle.example';
import ToggleData from '../../data/toggle.json';

function TogglePage() {
  return (
    <div>
      <HookDoc data={ToggleData}>
        <ToggleComponent />
      </HookDoc>
    </div>
  );
}

export default TogglePage;
