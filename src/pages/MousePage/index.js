import React from 'react';
import HookDoc from '../../components/hookdoc';
import MouseComponent from '../../examples/useMouse.example';
import MouseData from '../../data/mouse.json';

function MousePage() {
  return (
    <div>
      <HookDoc data={MouseData}>
        <MouseComponent />
      </HookDoc>
    </div>
  );
}

export default MousePage;
