import React from 'react';
import HookDoc from '../../components/hookdoc';
import EffectOnceComponent from '../../examples/useEffectOnce.example';
import EffectOnceData from '../../data/effect-once.json';

function EffectOncePage() {
  return (
    <div>
      <HookDoc data={EffectOnceData}>
        <EffectOnceComponent />
      </HookDoc>
    </div>
  );
}

export default EffectOncePage;
