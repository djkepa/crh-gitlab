import React from 'react';
import HookDoc from '../../components/hookdoc';
import TimeoutComponent from '../../examples/useTimeout.example';
import TimeoutData from '../../data/timeout.json';

function TimeoutPage() {
  return (
    <div>
      <HookDoc data={TimeoutData}>
        <TimeoutComponent />
      </HookDoc>
    </div>
  );
}

export default TimeoutPage;
