import React from 'react';
import HookDoc from '../../components/hookdoc';
import StatusComponent from '../../examples/useStatus.example';
import StatusData from '../../data/status.json';

function StatusPage() {
  return (
    <div>
      <HookDoc data={StatusData}>
        <StatusComponent />
      </HookDoc>
    </div>
  );
}

export default StatusPage;
