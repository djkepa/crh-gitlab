import React from 'react';
import HookDoc from '../../components/hookdoc';
import EventListenerComponent from '../../examples/useEventListener.example';
import EventListenerData from '../../data/event-listener.json';

function EventListenerPage() {
  return (
    <div>
      <HookDoc data={EventListenerData}>
        <EventListenerComponent />
      </HookDoc>
    </div>
  );
}

export default EventListenerPage;
