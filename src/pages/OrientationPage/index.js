import React from 'react';
import HookDoc from '../../components/hookdoc';
import OrientationComponent from '../../examples/useOrientation.example';
import OrientationData from '../../data/orientation.json';

function OrientationPage() {
  return (
    <div>
      <HookDoc data={OrientationData}>
        <OrientationComponent />
      </HookDoc>
    </div>
  );
}

export default OrientationPage;
