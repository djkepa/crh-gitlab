import React from 'react';
import HookDoc from '../../components/hookdoc';
import StepComponent from '../../examples/useStep.example';
import StepData from '../../data/step.json';

function StepPage() {
  return (
    <div>
      <HookDoc data={StepData}>
        <StepComponent />
      </HookDoc>
    </div>
  );
}

export default StepPage;
