import React from 'react';
import HookDoc from '../../components/hookdoc';
import WindowSizeComponent from '../../examples/useWindowSize.example';
import WindowSizeData from '../../data/window-size.json';

function WindowSizePage() {
  return (
    <div>
      <HookDoc data={WindowSizeData}>
        <WindowSizeComponent />
      </HookDoc>
    </div>
  );
}

export default WindowSizePage;
