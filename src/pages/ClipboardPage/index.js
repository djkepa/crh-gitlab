import React from 'react';
import HookDoc from '../../components/hookdoc';
import ClipboardComponent from '../../examples/useClipboard.example';
import ClipboardData from '../../data/clipboard.json';

function ClipboardPage() {
  return (
    <div>
      <HookDoc data={ClipboardData}>
        <ClipboardComponent />
      </HookDoc>
    </div>
  );
}

export default ClipboardPage;
