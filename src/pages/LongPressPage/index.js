import React from 'react';
import HookDoc from '../../components/hookdoc';
import LongPressComponent from '../../examples/useLongPress.example';
import LongPressData from '../../data/long-press.json';

function LongPressPage() {
  return (
    <div>
      <HookDoc data={LongPressData}>
        <LongPressComponent />
      </HookDoc>
    </div>
  );
}

export default LongPressPage;
