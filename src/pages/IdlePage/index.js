import React from 'react';
import HookDoc from '../../components/hookdoc';
import IdleComponent from '../../examples/useIdle.example';
import IdleData from '../../data/idle.json';

function IdlePage() {
  return (
    <div>
      <HookDoc data={IdleData}>
        <IdleComponent />
      </HookDoc>
    </div>
  );
}

export default IdlePage;
