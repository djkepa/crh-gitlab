import React from 'react';
import HookDoc from '../../components/hookdoc';
import OnScreenComponent from '../../examples/useOnScreen.example';
import OnScreenData from '../../data/on-screen.json';

function OnScreenPage() {
  return (
    <div>
      <HookDoc data={OnScreenData}>
        <OnScreenComponent />
      </HookDoc>
    </div>
  );
}

export default OnScreenPage;
