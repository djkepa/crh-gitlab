import React from 'react';
import HookDoc from '../../components/hookdoc';
import KeyPressComponent from '../../examples/useKeyPress.example';
import KeyPressData from '../../data/key-press.json';

function KeyPressPage() {
  return (
    <div>
      <HookDoc data={KeyPressData}>
        <KeyPressComponent />
      </HookDoc>
    </div>
  );
}

export default KeyPressPage;
