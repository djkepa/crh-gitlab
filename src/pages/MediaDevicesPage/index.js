import React from 'react';
import HookDoc from '../../components/hookdoc';
import MediaDevicesComponent from '../../examples/useMediaDevices.example';
import MediaDevicesData from '../../data/media-devices.json';

function MediaDevicesPage() {
  return (
    <div>
      <HookDoc data={MediaDevicesData}>
        <MediaDevicesComponent />
      </HookDoc>
    </div>
  );
}

export default MediaDevicesPage;
