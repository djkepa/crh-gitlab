import React from 'react';
import HookDoc from '../../components/hookdoc';
import DebounceComponent from '../../examples/useDebounce.example';
import DebounceData from '../../data/debounce.json';

function DebouncePage() {
  return (
    <div>
      <HookDoc data={DebounceData}>
        <DebounceComponent />
      </HookDoc>
    </div>
  );
}

export default DebouncePage;
