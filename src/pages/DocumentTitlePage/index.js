import React from 'react';
import HookDoc from '../../components/hookdoc';
import DocumentTitleComponent from '../../examples/useDocumentTitle.example';
import DocumentTitleData from '../../data/document-title.json';

function DocumentTitlePage() {
  return (
    <div>
      <HookDoc data={DocumentTitleData}>
        <DocumentTitleComponent />
      </HookDoc>
    </div>
  );
}

export default DocumentTitlePage;
