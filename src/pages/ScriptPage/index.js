import React from 'react';
import HookDoc from '../../components/hookdoc';
import ScriptComponent from '../../examples/useScript.example';
import ScriptData from '../../data/script.json';

function ScriptPage() {
  return (
    <div>
      <HookDoc
        hookName="timeout"
        data={ScriptData}
      >
        <ScriptComponent />
      </HookDoc>
    </div>
  );
}

export default ScriptPage;
