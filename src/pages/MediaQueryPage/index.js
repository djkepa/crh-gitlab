import React from 'react';
import HookDoc from '../../components/hookdoc';
import MediaQueryComponent from '../../examples/useMediaQuery.example';
import MediaQueryData from '../../data/media-query.json';

function MediaQueryPage() {
  return (
    <div>
      <HookDoc
        data={MediaQueryData}
        hookName="media-query"
      >
        <MediaQueryComponent />
      </HookDoc>
    </div>
  );
}

export default MediaQueryPage;
