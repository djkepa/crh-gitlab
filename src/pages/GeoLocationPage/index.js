import React from 'react';
import HookDoc from '../../components/hookdoc';
import GeoLocationComponent from '../../examples/useGeoLocation.example';
import GeoLocationData from '../../data/geo-location.json';

function GeoLocationPage() {
  return (
    <div>
      <HookDoc data={GeoLocationData}>
        <GeoLocationComponent />
      </HookDoc>
    </div>
  );
}

export default GeoLocationPage;
