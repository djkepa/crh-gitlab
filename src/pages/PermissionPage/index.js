import React from 'react';
import HookDoc from '../../components/hookdoc';
import PermissionComponent from '../../examples/usePermission.example';
import PermissionData from '../../data/permission.json';

function PermissionPage() {
  return (
    <div>
      <HookDoc data={PermissionData}>
        <PermissionComponent />
      </HookDoc>
    </div>
  );
}

export default PermissionPage;
