import React from 'react';
import HookDoc from '../../components/hookdoc';
import LockBodyScrollComponent from '../../examples/useLockBodyScroll.example';
import LockBodyScrollData from '../../data/lock-body-scroll.json';

function LockBodyScrollPage() {
  return (
    <div>
      <HookDoc data={LockBodyScrollData}>
        <LockBodyScrollComponent />
      </HookDoc>
    </div>
  );
}

export default LockBodyScrollPage;
