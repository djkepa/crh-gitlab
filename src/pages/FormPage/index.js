import React from 'react';
import HookDoc from '../../components/hookdoc';
import FormComponent from '../../examples/useForm.example';
import FormData from '../../data/form.json';

function FormPage() {
  return (
    <div>
      <HookDoc data={FormData}>
        <FormComponent />
      </HookDoc>
    </div>
  );
}

export default FormPage;
