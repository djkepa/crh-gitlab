import React from 'react';
import HookDoc from '../../components/hookdoc';
import UpdateEffectComponent from '../../examples/useUpdateEffect.example';
import UpdateEffectData from '../../data/update-effect.json';

function UpdateEffectPage() {
  return (
    <div>
      <HookDoc data={UpdateEffectData}>
        <UpdateEffectComponent />
      </HookDoc>
    </div>
  );
}

export default UpdateEffectPage;
