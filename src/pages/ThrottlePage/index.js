import React from 'react';
import HookDoc from '../../components/hookdoc';
import ThrottleComponent from '../../examples/useThrottle.example';
import ThrottleData from '../../data/throttle.json';

function ThrottlePage() {
  return (
    <div>
      <HookDoc data={ThrottleData}>
        <ThrottleComponent />
      </HookDoc>
    </div>
  );
}

export default ThrottlePage;
