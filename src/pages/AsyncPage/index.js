import React from 'react';
import { useAsync } from '@custom-react-hooks/all';
import asyncData from '../../data/async.json';
import HookDoc from '../../components/hookdoc';
import DataFetcher from '../../examples/useAsync.example';

function AsyncPage() {
  return (
    <div>
      <HookDoc data={asyncData}>
        <DataFetcher />
      </HookDoc>
    </div>
  );
}

export default AsyncPage;
