import React from 'react';
import HookDoc from '../../components/hookdoc';
import ImageLoadComponent from '../../examples/useImageLoad.example';
import ImageLoadData from '../../data/image-load.json';

function ImageLoadPage() {
  return (
    <div>
      <HookDoc data={ImageLoadData}>
        <ImageLoadComponent />
      </HookDoc>
    </div>
  );
}

export default ImageLoadPage;
