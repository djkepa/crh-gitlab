import React from 'react';
import HookDoc from '../../components/hookdoc';
import FetchComponent from '../../examples/useFetch.example';
import FetchData from '../../data/fetch.json';

function FetchPage() {
  return (
    <div>
      <HookDoc data={FetchData}>
        <FetchComponent />
      </HookDoc>
    </div>
  );
}

export default FetchPage;
