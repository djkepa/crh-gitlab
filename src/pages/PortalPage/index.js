import React from 'react';
import HookDoc from '../../components/hookdoc';
import PortalComponent from '../../examples/usePortal.example';
import PortalData from '../../data/portal.json';

function PortalPage() {
  return (
    <div>
      <HookDoc data={PortalData}>
        <PortalComponent />
      </HookDoc>
    </div>
  );
}

export default PortalPage;
