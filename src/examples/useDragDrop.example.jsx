import React, { useState, useCallback } from 'react';
// import { useDragDrop } from '@custom-react-hooks/all';

const DraggableItem = ({ id, bindDrag }) => {
  return (
    <div
      {...bindDrag(id)}
      style={{
        border: '2px solid #f9f871',
        padding: '10px',
        margin: '5px',
        cursor: 'grab',
        width: 'max-content',
        color: '#f9f871',
      }}
    >
      {id}
    </div>
  );
};

const DroppableArea = ({ id, bindDrop, isOver, children }) => {
  const style = {
    height: '100px',
    width: '100px',
    border: isOver ? '2px dashed #00989a' : '2px dashed #3860e8',
    padding: '10px',
    margin: '10px',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  };

  return (
    <div
      {...bindDrop(id)}
      style={style}
    >
      {children}
    </div>
  );
};

const DragDropComponent = () => {
  const [itemLocations, setItemLocations] = useState({ Item1: 'outside' });

  const handleDrop = (dragId, dropId) => {
    setItemLocations((prev) => ({ ...prev, [dragId]: dropId }));
  };

  const { state, bindDrag, bindDrop } = useDragDrop(handleDrop);

  const renderDraggableItem = (id, location) => {
    return itemLocations[id] === location ? (
      <DraggableItem
        id={id}
        bindDrag={bindDrag}
      />
    ) : null;
  };

  return (
    <div>
      {renderDraggableItem('Item1', 'outside')}

      <div className="btns">
        <DroppableArea
          id="Area1"
          bindDrop={bindDrop}
          isOver={state.isOver && state.overDropId === 'Area1'}
        >
          {renderDraggableItem('Item1', 'Area1')}
        </DroppableArea>

        <DroppableArea
          id="Area2"
          bindDrop={bindDrop}
          isOver={state.isOver && state.overDropId === 'Area2'}
        >
          {renderDraggableItem('Item1', 'Area2')}
        </DroppableArea>
      </div>
    </div>
  );
};

export default DragDropComponent;

const useDragDrop = (onDrop) => {
  const [state, setState] = useState({
    isDragging: false,
    isOver: false,
    draggedItemId: null,
    overDropId: null,
  });

  const handleDragStart = useCallback((e, dragId) => {
    e.dataTransfer.setData('text/plain', dragId);
    e.dataTransfer.effectAllowed = 'move';
    setState({ isDragging: true, isOver: false, draggedItemId: dragId, overDropId: null });
  }, []);

  const handleDragOver = useCallback(
    (e, dropId) => {
      e.preventDefault();
      if (state.overDropId !== dropId) {
        setState((s) => ({ ...s, isOver: true, overDropId: dropId }));
      }
    },
    [state.overDropId],
  );

  const handleDragEnter = useCallback((e, dropId) => {
    e.preventDefault();
    setState((s) => ({ ...s, isOver: true, overDropId: dropId }));
  }, []);

  const handleDragLeave = useCallback(
    (e, dropId) => {
      e.preventDefault();
      if (state.overDropId === dropId) {
        setState((s) => ({ ...s, isOver: false, overDropId: null }));
      }
    },
    [state.overDropId],
  );

  const handleDrop = useCallback(
    (e, dropId) => {
      e.preventDefault();
      const dragId = e.dataTransfer.getData('text/plain');
      setState({ isDragging: false, isOver: false, draggedItemId: dragId, overDropId: dropId });
      onDrop(dragId, dropId);
    },
    [onDrop],
  );

  return {
    state,
    bindDrag: (dragId) => ({
      draggable: true,
      onDragStart: (e) => handleDragStart(e, dragId),
    }),
    bindDrop: (dropId) => ({
      onDragOver: (e) => handleDragOver(e, dropId),
      onDragEnter: (e) => handleDragEnter(e, dropId),
      onDragLeave: (e) => handleDragLeave(e, dropId),
      onDrop: (e) => handleDrop(e, dropId),
    }),
  };
};
