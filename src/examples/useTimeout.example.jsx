import React, { useState } from 'react';
import { useTimeout } from '@custom-react-hooks/all';

const TimeoutComponent = () => {
  const [message, setMessage] = useState('');
  const showMessage = () => setMessage('Hello! The timeout has completed.');

  const { isActive, reset, clear } = useTimeout(showMessage, 3000);

  const handleStart = () => {
    setMessage('');
    reset();
  };

  const resetMessage = () => {
    clear();
    setMessage('');
  };

  return (
    <div className="center">
      <h2>Timeout Example</h2>
      <div className="btns">
        <button
          className="btn-green"
          onClick={handleStart}
          disabled={isActive}
        >
          {isActive ? 'Timeout is active...' : 'Start'}
        </button>
        <button
          className="btn-red"
          onClick={resetMessage}
          disabled={!isActive}
        >
          Clear
        </button>
      </div>

      <p>{message}</p>
    </div>
  );
};

export default TimeoutComponent;
