import React, { useEffect, useRef, useState } from 'react';
import { useFrame } from 'react-frame-component';
// import { useEventListener } from '@custom-react-hooks/all';

const EventListenerComponent = () => {
  const [count, setCount] = useState(0);

  const handleKeyDown = (event) => {
    if (event.key === 'ArrowRight') {
      setCount((prevCount) => prevCount + 1);
    } else if (event.key === 'ArrowLeft') {
      setCount((prevCount) => prevCount - 1);
    }
  };

  useEventListener('keydown', handleKeyDown);

  const getColor = () => {
    if (count < 0) return '#ff5868';
    if (count > 0) return '#00989a';
    return '#f9f871';
  };

  return (
    <div>
      <h2>Press the Arrow Left/Right keys to change the counter.</h2>
      <p className="center">(If is not working, click me!)</p>
      <p className="center">
        Counter: <span style={{ color: getColor() }}>{count}</span>
      </p>
    </div>
  );
};

export default EventListenerComponent;

function useEventListener(eventName, handler, element, options, condition = true) {
  const { document, window } = useFrame();
  const savedHandler = useRef();

  useEffect(() => {
    savedHandler.current = handler;
  }, [handler]);

  useEffect(() => {
    if (!condition) return;

    const targetElement = element?.current || window;
    if (!targetElement.addEventListener) return;

    const eventListener = (event) => savedHandler.current && savedHandler.current(event);

    const events = Array.isArray(eventName) ? eventName : [eventName];
    events.forEach((e) => targetElement.addEventListener(e, eventListener, options));

    return () => {
      events.forEach((e) => targetElement.removeEventListener(e, eventListener, options));
    };
  }, [eventName, element, options, condition]);
}
