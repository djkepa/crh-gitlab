import React, { useEffect, useRef, useState } from 'react';
import { useLongPress } from '@custom-react-hooks/all';

const LongPressTestComponent = () => {
  const [status, setStatus] = useState('Ready');

  const longPressCallback = () => {
    setStatus('Finished');
  };

  const longPressEvents = useLongPress(longPressCallback, {
    threshold: 500,
    onStart: () => setStatus('Started'),
    onFinish: () => setStatus('Finished'),
    onCancel: () => setStatus('Cancelled'),
  });

  const color =
    status === 'Started'
      ? 'yellow'
      : status === 'Cancelled'
      ? 'red'
      : status === 'Finished'
      ? 'green'
      : 'blue';

  return (
    <div className="center">
      <button
        {...longPressEvents}
        className={`btn-${color}`}
      >
        Press and Hold
      </button>
      <p>
        Status: <span className={`c-${color}`}>{status}</span>
      </p>
    </div>
  );
};

export default LongPressTestComponent;
