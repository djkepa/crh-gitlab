import React from 'react';
import { useFrame } from 'react-frame-component';
import { useWindowSize } from '../components/useWindow';

const WindowSizeComponent = () => {
  const { document, window } = useFrame();
  const { width, height } = useWindowSize(100, window, document);

  return (
    <div>
      <h2>Current Window Size:</h2>
      <div className="item-between">
        <p>
          Width: <span className="c-green">{width}</span>
        </p>
        <p>
          Height: <span className="c-green">{height}</span>
        </p>
      </div>
    </div>
  );
};
export default WindowSizeComponent;
