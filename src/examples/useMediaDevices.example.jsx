import React from 'react';
import useMediaDevices from '../components/useDevices';
// import { useMediaDevices } from '@custom-react-hooks/all';

const MediaDevicesComponent = () => {
  const { devices, isLoading, error } = useMediaDevices(false);

  if (isLoading) {
    return <div>Loading devices...</div>;
  }

  if (error) {
    return <div>Error: {error}</div>;
  }

  return (
    <div className="media-devices">
      <h2>Available Media Devices</h2>
      {devices.length === 0 ? (
        <div>No devices found.</div>
      ) : (
        <ul>
          {devices.map((device) => (
            <li key={device.id}>
              <strong>{device.kind}:</strong> {device.label}
            </li>
          ))}
        </ul>
      )}
    </div>
  );
};

export default MediaDevicesComponent;
