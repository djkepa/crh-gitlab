import React, { useState } from 'react';
import { useClipboard } from '@custom-react-hooks/all';

const ClipboardExample = () => {
  const { copyToClipboard, pasteFromClipboard, state } = useClipboard();
  const [textToCopy, setTextToCopy] = useState('');
  const [pastedText, setPastedText] = useState('');

  const handleCopy = async () => {
    await copyToClipboard(textToCopy);
  };

  const handlePaste = async () => {
    const text = await pasteFromClipboard();
    setPastedText(text);
  };

  return (
    <div className="clipboard">
      <input
        type="text"
        value={textToCopy}
        onChange={(e) => setTextToCopy(e.target.value)}
        placeholder="Text to copy"
      />
      <div className="item-between">
        <button
          className="copy"
          onClick={handleCopy}
        >
          Copy to Clipboard
        </button>
        <button
          className="paste"
          onClick={handlePaste}
        >
          Paste from Clipboard
        </button>
      </div>

      {state.success && <p>Operation successful!</p>}
      {state.error && <p>Error: {state.error}</p>}

      <p>
        Pasted Text: <span style={{ color: '#f9f871' }}>{pastedText}</span>
      </p>
    </div>
  );
};

export default ClipboardExample;
