import React, { useState, useEffect, useCallback } from 'react';
import { useFrame } from 'react-frame-component';
// import { useMediaQuery } from '@custom-react-hooks/all';

const MediaQueryComponent = () => {
  const isWide = useMediaQuery('(min-width: 300px)');

  const style = {
    padding: '20px',
    color: 'white',
    textAlign: 'center',
    backgroundColor: isWide ? '#1dd0d3' : '#ff896f',
    borderRadius: '10px',
  };

  return (
    <div style={style}>
      {isWide ? 'Wide viewport detected' : 'Narrow viewport detected'}
      <br />
      <span>
        <i>Resize to see the effect</i>
      </span>
    </div>
  );
};

export default MediaQueryComponent;

const getMatches = (mediaQuery) => {
  if (typeof window === 'undefined' || typeof window.matchMedia === 'undefined') {
    return false;
  }

  return window.matchMedia(mediaQuery).matches;
};

function useMediaQuery(query) {
  const { window, document } = useFrame();
  const [matches, setMatches] = useState(getMatches(query));

  const handleChange = useCallback((event) => {
    setMatches(event.matches);
  }, []);

  useEffect(() => {
    if (typeof window === 'undefined' || typeof window.matchMedia === 'undefined') {
      return;
    }

    const mediaQueryList = window.matchMedia(query);
    mediaQueryList.addEventListener('change', handleChange);

    return () => {
      mediaQueryList.removeEventListener('change', handleChange);
    };
  }, [query, handleChange]);

  return matches;
}
