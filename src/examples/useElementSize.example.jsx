import React from 'react';
import { useElementSize } from '../components/useElement';

const ElementSizeComponent = () => {
  const [setRef, size] = useElementSize();

  return (
    <div className="element-size">
      <div
        ref={setRef}
        className="resize"
      >
        <p>Drag from the bottom-right corner to resize.</p>
      </div>
      <br />
      <div>Current Size:</div>
      <div className="btns">
        <p>
          Width: <span style={{ color: '#ff5868' }}>{size.width}px</span>
        </p>
        <p>
          Height: <span style={{ color: '#ff5868' }}>{size.height}px</span>
        </p>
      </div>
    </div>
  );
};

export default ElementSizeComponent;
