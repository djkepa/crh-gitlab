import React from 'react';
import { usePermission } from '@custom-react-hooks/all';
import { useState } from 'react';

const PermissionComponent = () => {
  const [selectedPermission, setSelectedPermission] = useState('geolocation');
  const { state, isLoading, error } = usePermission(selectedPermission);

  return (
    <div>
      <h2>Check Browser Permission Status</h2>
      <label htmlFor="permission-select">Choose a permission: </label>
      <select
        id="permission-select"
        value={selectedPermission}
        onChange={(e) => setSelectedPermission(e.target.value)}
      >
        <option value="geolocation">Geolocation</option>
        <option value="notifications">Notifications</option>
        <option value="microphone">Microphone</option>
        <option value="camera">Camera</option>
      </select>

      <div>
        {isLoading ? (
          <p>Loading...</p>
        ) : (
          <p>
            Permission status for {selectedPermission}:
            <span style={{ color: 'lightcoral' }}> {state}</span>
          </p>
        )}
        {error && <p>Error: {error}</p>}
      </div>
    </div>
  );
};

export default PermissionComponent;
