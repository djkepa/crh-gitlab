import { useKeyPress } from '@custom-react-hooks/all';
import React from 'react';

const KeyPressComponent = () => {
  const enterPressed = useKeyPress('Enter');

  return (
    <div style={{ width: '300px', display: 'flex', alignItems: 'center', flexDirection: 'column' }}>
      <p>Press the "Enter" key</p>
      <button
        className={`keyboard-key ${enterPressed ? 'pressed' : ''}`}
        id="enterButton"
      >
        Enter
      </button>
      {enterPressed && <p>You are pressing the "Enter" key!</p>}
    </div>
  );
};

export default KeyPressComponent;
