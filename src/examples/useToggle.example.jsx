import React from 'react';
import { useToggle } from '@custom-react-hooks/all';

const ToggleComponent = () => {
  const { value, toggle, setTrue, setFalse } = useToggle(false);

  return (
    <div className="toggle">
      <div className="btns">
        <button onClick={toggle}>Toggle</button>
        <button
          onClick={setTrue}
          className="btn btn-green"
        >
          Set True
        </button>
        <button
          className="btn btn-red"
          onClick={setFalse}
        >
          Set False
        </button>
      </div>
      {value && <h2 style={{ textAlign: 'center' }}>Message Visible</h2>}
    </div>
  );
};

export default ToggleComponent;
