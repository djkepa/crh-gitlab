import React, { useState } from 'react';
import { useDebounce } from '@custom-react-hooks/all';

const DebounceComponent = () => {
  const [inputValue, setInputValue] = useState('');
  const [debouncedValue, setDebouncedValue] = useState('');

  const [updateDebouncedValue] = useDebounce(
    (val) => {
      setDebouncedValue(val);
    },
    1000,
    { maxWait: 2000 },
  );

  const handleChange = (e) => {
    const value = e.target.value;
    setInputValue(value);
    updateDebouncedValue(value);
  };

  return (
    <div className="debounce">
      <input
        type="text"
        value={inputValue}
        onChange={handleChange}
        placeholder="Type here..."
      />
      <p>
        Debounced Value: <br />
        <span>{debouncedValue}</span>
      </p>
    </div>
  );
};

export default DebounceComponent;
