import { useHover } from '@custom-react-hooks/all';

function HoverComponent() {
  const { ref, isHovered } = useHover();

  return (
    <div
      ref={ref}
      data-testid="hover-component"
    >
      <h3 style={{ cursor: 'pointer', backgroundColor: `${isHovered ? '#ff589c' : '#3860e8'}` }}>
        {isHovered ? 'Hovered' : 'Hover Me!'}
      </h3>
    </div>
  );
}

export default HoverComponent;
