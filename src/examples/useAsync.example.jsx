import { useAsync } from '@custom-react-hooks/all';
import React, { useEffect, useState } from 'react';
import { useFrame } from 'react-frame-component';

const fetchData = async (endpoint) => {
  const response = await fetch(endpoint);
  if (!response.ok) {
    throw new Error(`Failed to fetch from ${endpoint}`);
  }
  return response.json();
};

const AsyncComponent = () => {
  const [endpoint, setEndpoint] = useState('https://jsonplaceholder.typicode.com/todos/1');
  const [simulateError, setSimulateError] = useState(false);
  const { execute, status, value: data, error } = useAsync(() => fetchData(endpoint), false);

  const handleFetch = () => {
    if (simulateError) {
      setEndpoint('https://jsonplaceholder.typicode.com/todos1/1');
    }
    execute();
  };

  return (
    <div className="async">
      <input
        type="text"
        value={endpoint}
        onChange={(e) => setEndpoint(e.target.value)}
        placeholder="Enter API endpoint"
      />
      <button onClick={handleFetch}>Fetch Data</button>
      <div>
        <label>
          <input
            type="checkbox"
            checked={simulateError}
            onChange={() => setSimulateError(!simulateError)}
          />
          Simulate Error
        </label>
      </div>

      {status === 'pending' && <p>Loading...</p>}
      {status === 'error' && <p>Error: {error?.message}</p>}
      {status === 'success' && (
        <p class="result">
          <b>Data:</b>
          <pre>{JSON.stringify(data, null, 2)}</pre>
        </p>
      )}
    </div>
  );
};

export default AsyncComponent;
