import React, { useCallback, useEffect, useMemo, useRef, useState } from 'react';
import { useFrame } from 'react-frame-component';

const Tooltip = ({ mousePosition, isVisible }) => {
  const { window, document } = useFrame();
  const tooltipRef = useRef(null);

  const positionStyles = useMemo(() => {
    const style = { display: isVisible ? 'block' : 'none' };

    if (tooltipRef.current) {
      const tooltipRect = tooltipRef.current.getBoundingClientRect();

      let newX = mousePosition.x;
      let newY = mousePosition.y;

      if (newX + tooltipRect.width > window.innerWidth) {
        newX -= tooltipRect.width;
      }

      if (newY + tooltipRect.height > window.innerHeight) {
        newY -= tooltipRect.height;
      }

      style.left = `${newX}px`;
      style.top = `${newY}px`;
    }

    return style;
  }, [mousePosition, isVisible]);

  return (
    <div
      ref={tooltipRef}
      style={positionStyles}
    >
      X: <span style={{ color: '#ff896f' }}>{mousePosition.x}</span>, Y:{' '}
      <span style={{ color: '#ff896f' }}>{mousePosition.y}</span>
    </div>
  );
};

const MouseComponent = () => {
  const boxRef = useRef(null);
  const [isTooltipVisible, setIsTooltipVisible] = useState(false);
  const tooltipOptions = {
    offsetX: 15,
    offsetY: 15,
    avoidEdges: true,
    tooltipWidth: 120,
    tooltipHeight: 50,
  };
  const mousePosition = useMouse(boxRef, tooltipOptions);

  const handleMouseEnter = () => setIsTooltipVisible(true);
  const handleMouseLeave = () => setIsTooltipVisible(false);

  return (
    <div className="center">
      <div
        ref={boxRef}
        style={{
          width: '180px',
          height: '180px',
          backgroundColor: '#ff896f',
          display: 'grid',
          justifyContent: 'center',
          alignContent: 'center',
          borderRadius: '10px',
          cursor: 'pointer',
          padding: '10px',
        }}
        onMouseEnter={handleMouseEnter}
        onMouseLeave={handleMouseLeave}
      >
        Hover over this box
      </div>
      <Tooltip
        mousePosition={mousePosition}
        isVisible={isTooltipVisible}
      />
    </div>
  );
};

export default MouseComponent;

function useMouse(
  ref,
  options = { offsetX: 10, offsetY: 10, avoidEdges: false, relativeToWindow: false },
) {
  const { window, document } = useFrame();
  const [mousePosition, setMousePosition] = useState({
    x: 0,
    y: 0,
    position: 'bottomRight',
  });

  const updateMousePosition = useCallback(
    (ev) => {
      const {
        offsetX,
        offsetY,
        avoidEdges,
        tooltipWidth = 100,
        tooltipHeight = 50,
        relativeToWindow,
      } = options;

      let newX = ev.clientX + offsetX;
      let newY = ev.clientY + offsetY;
      let newPosition = 'bottomRight';

      if (avoidEdges) {
        if (relativeToWindow) {
          if (newX + tooltipWidth > window.innerWidth) {
            newX = ev.clientX - tooltipWidth - offsetX;
            newPosition = 'bottomLeft';
          }
          if (newY + tooltipHeight > window.innerHeight) {
            newY = ev.clientY - tooltipHeight - offsetY;
            newPosition = newPosition === 'bottomLeft' ? 'topLeft' : 'topRight';
          }
        } else {
          const rect = ref.current?.getBoundingClientRect();
          if (rect) {
            if (newX + tooltipWidth > rect.right) {
              newX = ev.clientX - tooltipWidth - offsetX;
              newPosition = 'bottomLeft';
            }
            if (newY + tooltipHeight > rect.bottom) {
              newY = ev.clientY - tooltipHeight - offsetY;
              newPosition = newPosition === 'bottomLeft' ? 'topLeft' : 'topRight';
            }
          }
        }
      }

      setMousePosition({ x: newX, y: newY, position: newPosition });
    },
    [options, ref],
  );

  useEffect(() => {
    const handleMouseMove = (ev) => {
      if (options.relativeToWindow || (ref.current && ref.current.contains(ev.target))) {
        updateMousePosition(ev);
      }
    };

    document.addEventListener('mousemove', handleMouseMove);

    return () => {
      document.removeEventListener('mousemove', handleMouseMove);
    };
  }, [updateMousePosition, options.relativeToWindow, ref]);

  return mousePosition;
}
