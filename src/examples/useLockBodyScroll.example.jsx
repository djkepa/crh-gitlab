import { useLockBodyScroll } from '@custom-react-hooks/all';
import React, { useState } from 'react';

const LockBodyScrollComponent = () => {
  const [isModalOpen, setIsModalOpen] = useState(false);
  useLockBodyScroll(isModalOpen);

  return (
    <div className="lock-body-scroll center">
      <p>{isModalOpen ? 'Scroll hidden' : 'Click on the button to hide scroll'}</p>
      <button onClick={() => setIsModalOpen(true)}>Open Modal</button>
      {isModalOpen && (
        <div className="modal">
          <div style={{ background: 'white', padding: 20, margin: 50 }}>
            <p>Modal Content</p>
            <button onClick={() => setIsModalOpen(false)}>Close Modal</button>
          </div>
        </div>
      )}
    </div>
  );
};

export default LockBodyScrollComponent;
