import { useIdle } from '@custom-react-hooks/all';
import { MoonIcon, SunIcon } from '@heroicons/react/24/solid';
import React from 'react';

const IdleComponent = () => {
  const isIdle = useIdle(1000);

  return (
    <div className="center">
      {isIdle ? (
        <div className="btns">
          <MoonIcon style={{ color: '#f9f871', width: '20px' }} />
          <p style={{ color: '#f9f871' }}>Away</p>
        </div>
      ) : (
        <div className="btns">
          <SunIcon style={{ color: '#00989a', width: '20px' }} />
          <p style={{ color: '#00989a' }}>Online</p>
        </div>
      )}
      <br />
      <p>To see the effect, do not move the mouse or touch the keyboard!</p>
    </div>
  );
};

export default IdleComponent;
