import React, { useRef } from 'react';
import useImageLoad from '../components/useImageOnLoad';

const thumbnailSrc = 'https://janvey.com/media/catalog/product/placeholder/default/placeholder.png';
const fullSrc =
  'https://gcdn.thunderstore.io/live/repository/icons/TeamThom-training-1.0.0.png.256x256_q95_crop.png';

const ImageLoadComponent = () => {
  const thumbnailOnlyRef = useRef(null);
  const lazyLoadRef = useRef(null);

  const thumbnailOnlyState = useImageLoad({ thumbnailSrc, fullSrc: '' }, thumbnailOnlyRef);
  const lazyLoadState = useImageLoad({ thumbnailSrc, fullSrc, lazyLoad: true }, lazyLoadRef);

  return (
    <div>
      <div>
        <h3>Thumbnail Only</h3>
        {thumbnailOnlyState.isLoading && <p>Loading thumbnail...</p>}
        {thumbnailOnlyState.hasError && <p>Error loading thumbnail.</p>}
        <img
          ref={thumbnailOnlyRef}
          src={thumbnailOnlyState.src}
          alt="Thumbnail Only"
        />
      </div>
      <p>Scroll down to trigger lazy load</p>

      <div style={{ marginTop: '300px' }}>
        <h3>Lazy Loading Image</h3>
        {lazyLoadState.isLoading && <p>Loading image...</p>}
        {lazyLoadState.hasError && <p>Error loading image.</p>}
        <img
          ref={lazyLoadRef}
          src={lazyLoadState.src}
          alt="Lazy Loading"
        />
      </div>
    </div>
  );
};

export default ImageLoadComponent;
