import React from 'react';
import { useStep } from '@custom-react-hooks/all';

const StepComponent = ({ totalSteps, initialStep, loop }) => {
  const { currentStep, nextStep, prevStep, reset } = useStep({ initialStep, totalSteps, loop });

  return (
    <div className="center">
      <h2>Current Step: {currentStep}</h2>
      <div className="btns">
        <button onClick={prevStep}>Previous</button>
        <button onClick={nextStep}>Next</button>
      </div>
      <button
        className="red-btn"
        onClick={reset}
      >
        Reset
      </button>
    </div>
  );
};

export default StepComponent;
