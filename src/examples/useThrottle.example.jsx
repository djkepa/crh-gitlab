import React, { useState } from 'react';
import { useThrottle } from '@custom-react-hooks/all';

const ThrottleComponent = () => {
  const [inputValue, setInputValue] = useState('');
  const throttledValue = useThrottle(inputValue, 1000);

  const handleChange = (event) => {
    setInputValue(event.target.value);
  };

  return (
    <div>
      <h2>Throttled Input Example</h2>
      <input
        type="text"
        value={inputValue}
        onChange={handleChange}
        placeholder="Type here..."
      />
      <p>Input Value: {inputValue}</p>
      <p>Throttled Value: {throttledValue}</p>
    </div>
  );
};

export default ThrottleComponent;
