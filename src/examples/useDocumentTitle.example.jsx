import React, { useState } from 'react';
import { useDocumentTitle } from '@custom-react-hooks/all';

const DocumentTitleComponent = () => {
  const [title, setTitle] = useState('My Awesome Page');
  useDocumentTitle(title);

  return (
    <div className="document-title">
      <h2>Welcome to My Awesome Page</h2>
      <p>Check document title when you change title</p>
      <button
        onClick={(e) => {
          setTitle('Clicked on the button');
        }}
      >
        Click on the button
      </button>
      <button
        onClick={(e) => {
          setTitle('My Awesome Page');
        }}
      >
        Restore title name
      </button>
    </div>
  );
};
export default DocumentTitleComponent;
