import React, { useState } from 'react';
import { useFetch } from '@custom-react-hooks/all';

const FetchComponent = () => {
  const [url, setUrl] = useState('https://jsonplaceholder.typicode.com/users/1');
  const { data, loading, error, fetchData } = useFetch(url, { manual: true });

  const handleChange = (event) => {
    setUrl(event.target.value);
  };

  const handleFetch = () => {
    fetchData();
  };

  return (
    <div>
      <input
        type="text"
        value={url}
        onChange={handleChange}
        style={{ width: '100%', marginBottom: '10px' }}
      />
      <button
        onClick={handleFetch}
        disabled={loading}
      >
        Fetch Data
      </button>

      {loading && <p>Loading...</p>}
      {error && <p>Error: {error.message}</p>}
      {data && (
        <div>
          <p>Data:</p>
          <pre>{JSON.stringify(data, null, 2)}</pre>
        </div>
      )}
    </div>
  );
};

export default FetchComponent;
