import React, { useState } from 'react';
import { useStorage } from '@custom-react-hooks/all';
import { XCircleIcon } from '@heroicons/react/24/solid';

const StorageList = ({ storageType }: { storageType: 'local' | 'session' }) => {
  const [items, setItems] = useStorage(`${storageType}-items`, [], storageType);

  const addItem = (item) => {
    setItems((prevItems) => [...prevItems, item]);
  };

  const removeItem = (index) => {
    setItems((prevItems) => prevItems.filter((_, i) => i !== index));
  };

  return (
    <div className="center">
      <input
        type="text"
        onKeyDown={(e) => {
          if (e.key === 'Enter' && e.currentTarget.value) {
            addItem(e.currentTarget.value);
            e.currentTarget.value = '';
          }
        }}
        placeholder={`Add to ${storageType} storage`}
      />
      <h2>{storageType === 'local' ? 'LocalStorage' : 'SessionStorage'} List</h2>
      <ol>
        {items.map((item, index) => (
          <li
            className="storage-item"
            key={index}
          >
            {item} <XCircleIcon onClick={() => removeItem(index)} />
          </li>
        ))}
      </ol>
    </div>
  );
};

const StorageComponent = () => (
  <div className="storage">
    <StorageList storageType="local" />
    <StorageList storageType="session" />
  </div>
);

export default StorageComponent;
