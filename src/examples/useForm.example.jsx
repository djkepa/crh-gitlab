import React, { useState } from 'react';
import { useForm } from '@custom-react-hooks/all';

const FormComponent = () => {
  const initialValues = { username: '', email: '' };
  const [submitting, setSubmitting] = useState(false);
  const [submitResult, setSubmitResult] = useState('');

  const validate = (values) => {
    const errors = {};
    if (!values.username) errors.username = 'Username is required';
    if (!values.email) errors.email = 'Email is required';
    return errors;
  };

  const { values, errors, touched, handleChange, handleBlur, handleSubmit, resetForm } = useForm(
    initialValues,
    validate,
  );

  const onSubmit = async () => {
    setSubmitting(true);
    console.log('Form submitted:', values);

    setTimeout(() => {
      setSubmitResult('Form submitted successfully!');
      setSubmitting(false);
    }, 2000);
  };

  return (
    <>
      {submitting && <div>Loading...</div>}
      {!submitting && submitResult && <div>{submitResult}</div>}
      <form
        onSubmit={(e) => handleSubmit(e, onSubmit)}
        className="form"
      >
        <div className="input-field">
          <label htmlFor="username">Username</label>
          <input
            id="username"
            name="username"
            value={values.username}
            onChange={handleChange}
            onBlur={handleBlur}
          />
          {touched.username && errors.username && <span className="error">{errors.username}</span>}
        </div>

        <div className="input-field">
          <label htmlFor="email">Email</label>
          <input
            id="email"
            name="email"
            value={values.email}
            onChange={handleChange}
            onBlur={handleBlur}
          />
          {touched.email && errors.email && <span className="error">{errors.email}</span>}
        </div>

        <div className="pair">
          <button
            type="submit"
            disabled={submitting}
          >
            Submit
          </button>
          <button
            type="button"
            onClick={resetForm}
            disabled={submitting}
          >
            Reset
          </button>
        </div>
      </form>
    </>
  );
};

export default FormComponent;
