import React, { useCallback, useRef, useState, useEffect } from 'react';
import { useFrame } from 'react-frame-component';

const OrientationComponent = () => {
  const elementRef1 = useRef(null);
  const elementRef2 = useRef(null);

  const elementOrientation1 = useOrientation(elementRef1, false);
  const elementOrientation2 = useOrientation(elementRef2, false);
  const windowOrientation = useOrientation(undefined, true);

  return (
    <div>
      <div
        ref={elementRef1}
        style={{
          width: '100px',
          height: '100px',
          backgroundColor: 'lightblue',
          margin: '10px',
          padding: '10px',
          borderRadius: '5px',
          color: '#18181d',
          fontWeight: '500',
          fontSize: '14px',
          transform: 'rotate(10deg)',
        }}
      >
        <strong>Element 1: </strong> {elementOrientation1.elementOrientation}
        <br />
        <strong>Aspect Ratio:</strong>
        {elementOrientation1.aspectRatio ? elementOrientation1.aspectRatio.toFixed(2) : 0}
      </div>
      <div
        ref={elementRef2}
        style={{
          width: '200px',
          height: '50px',
          backgroundColor: 'lightgreen',
          margin: '10px',
          padding: '10px',
          borderRadius: '5px',
          color: '#18181d',
          fontWeight: '500',
          fontSize: '14px',
        }}
      >
        <strong>Element 2: </strong>
        {elementOrientation2.elementOrientation}
        <br />
        <strong>Aspect Ratio:</strong>
        {elementOrientation2.aspectRatio ? elementOrientation2.aspectRatio.toFixed(2) : 0}
      </div>
      <p>Window Orientation: {windowOrientation.type}</p>
    </div>
  );
};

export default OrientationComponent;

function useOrientation(elementRef, trackWindow = false) {
  const getOrientation = () => {
    const orientationData = {
      angle: 0,
      type: undefined,
      aspectRatio: undefined,
      elementOrientation: undefined,
    };

    if (trackWindow && typeof window !== 'undefined' && window.screen.orientation) {
      const { angle, type } = window.screen.orientation;
      orientationData.angle = angle;
      orientationData.type = type;
    }

    if (!trackWindow && elementRef?.current) {
      const { offsetWidth, offsetHeight } = elementRef.current;
      orientationData.aspectRatio = offsetWidth / offsetHeight;
      orientationData.elementOrientation = offsetWidth > offsetHeight ? 'landscape' : 'portrait';
    }

    return orientationData;
  };

  const [orientation, setOrientation] = useState(getOrientation);

  const handleOrientationChange = useCallback(() => {
    setOrientation(getOrientation());
  }, [elementRef, trackWindow]);

  useEffect(() => {
    handleOrientationChange();
    window.addEventListener('resize', handleOrientationChange);
    if (trackWindow) {
      window.addEventListener('orientationchange', handleOrientationChange);
    }

    return () => {
      window.removeEventListener('resize', handleOrientationChange);
      if (trackWindow) {
        window.removeEventListener('orientationchange', handleOrientationChange);
      }
    };
  }, [handleOrientationChange, trackWindow]);

  return orientation;
}
