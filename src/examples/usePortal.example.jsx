import React from 'react';
import { usePortal } from '@custom-react-hooks/all';

const PortalComponent = () => {
  const { openPortal, closePortal, isOpen } = usePortal();

  return (
    <div>
      <button onClick={openPortal}>Open Portal</button>
      <button
        className="red-btn"
        onClick={closePortal}
      >
        Close Portal
      </button>
      {isOpen && <div className="modal">This is portal content</div>}
    </div>
  );
};

export default PortalComponent;
