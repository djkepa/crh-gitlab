import React, { useState } from 'react';
import { useEffectOnce } from '@custom-react-hooks/all';

const EffectOnceComponent = () => {
  const [fibonacciSequence, setFibonacciSequence] = useState([]);

  const calculateFibonacci = (n) => {
    let sequence = [0, 1];
    for (let i = 2; i < n; i++) {
      sequence[i] = sequence[i - 1] + sequence[i - 2];
    }
    return sequence.slice(0, n);
  };

  useEffectOnce(() => {
    const sequence = calculateFibonacci(5);
    setFibonacciSequence(sequence);
  });

  return (
    <div>
      <p>First {5} numbers in the Fibonacci sequence:</p>
      <ul>
        {fibonacciSequence.map((number, index) => (
          <li key={index}>{number}</li>
        ))}
      </ul>
    </div>
  );
};

export default EffectOnceComponent;
