import { useGeoLocation } from '@custom-react-hooks/all';

function GeoLocationComponent() {
  const { loading, coordinates, error, isWatching } = useGeoLocation();

  return (
    <div>
      <h1>GeoLocation Component</h1>
      {loading && <p>Loading...</p>}
      {!loading && error && <p>Error: {error.message}</p>}
      {!loading && !error && coordinates && (
        <div>
          <p>
            Latitude: <span style={{ color: '#f9f871' }}>{coordinates.latitude}</span>
          </p>
          <p>
            Longitude: <span style={{ color: '#f9f871' }}>{coordinates.longitude}</span>
          </p>
        </div>
      )}
      <p>
        Watching: <span style={{ color: '#ff896f' }}>{isWatching ? 'Yes' : 'No'}</span>
      </p>
    </div>
  );
}

export default GeoLocationComponent;
