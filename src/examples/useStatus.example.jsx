import React from 'react';
import { useStatus } from '@custom-react-hooks/all';

const StatusComponent = () => {
  const { online, downlink, effectiveType, rtt } = useStatus();

  return (
    <div>
      <h1>Network Status</h1>
      <p style={{ color: online ? '#1dd0d3' : '#ff5868' }}>{online ? 'Online' : 'Offline'}</p>
      {downlink && (
        <p>
          Downlink Speed:{' '}
          <span style={{ color: online ? '#1dd0d3' : '#ff5868' }}> {downlink} Mbps</span>
        </p>
      )}
      {effectiveType && (
        <p>
          Effective Type:{' '}
          <span style={{ color: online ? '#1dd0d3' : '#ff5868' }}> {effectiveType}</span>
        </p>
      )}
      {rtt && (
        <p>
          RTT: <span style={{ color: online ? '#1dd0d3' : '#ff5868' }}> {rtt} ms</span>
        </p>
      )}
    </div>
  );
};

export default StatusComponent;
