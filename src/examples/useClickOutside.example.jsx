import React, { useState, useRef, useEffect } from 'react';
import { useFrame } from 'react-frame-component';

const Modal = ({ onClose }) => {
  const modalRef = useRef(null);

  useClickOutside(modalRef, onClose);

  return (
    <div
      className="modal"
      ref={modalRef}
    >
      <p>Modal content goes here</p>
      <button onClick={onClose}>Close Modal</button>
    </div>
  );
};

const ClickOutsideComponent = () => {
  const [isModalOpen, setModalOpen] = useState(false);

  const openModal = (e) => {
    e.stopPropagation();
    if (!isModalOpen) {
      setModalOpen(true);
    }
  };

  const closeModal = () => setModalOpen(false);

  return (
    <div>
      <button onClick={(e) => openModal(e)}>Open Modal</button>
      {isModalOpen && <Modal onClose={closeModal} />}
    </div>
  );
};

export default ClickOutsideComponent;

function useClickOutside(
  refs,
  callback,
  events = ['mousedown', 'touchstart'],
  enableDetection = true,
  ignoreRefs = [],
) {
  const { document } = useFrame();
  useEffect(() => {
    const listener = (event) => {
      if (!enableDetection) return;

      const target = event.target;

      const isIgnored = ignoreRefs.some((ref) => ref.current?.contains(target));
      if (isIgnored) return;

      const isOutside = Array.isArray(refs)
        ? refs.every((ref) => !ref.current?.contains(target))
        : !refs.current?.contains(target);

      if (isOutside) {
        callback(event);
      }
    };

    events.forEach((event) => {
      document.addEventListener(event, listener);
    });

    return () => {
      events.forEach((event) => {
        document.removeEventListener(event, listener);
      });
    };
  }, [refs, callback, events, enableDetection, ignoreRefs]);
}
