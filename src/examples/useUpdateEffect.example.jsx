import { useEffect, useRef, useState, DependencyList, EffectCallback } from 'react';

const UpdateEffectComponent = () => {
  const [count, setCount] = useState(0);
  const [message, setMessage] = useState('');

  useUpdateEffect(() => {
    setMessage(`Effect ran at count: ${count}`);
  }, [count]);

  return (
    <div>
      <button onClick={() => setCount((c) => c + 1)}>Increment</button>
      <p>Count: {count}</p>
      <p>{message}</p>
    </div>
  );
};

export default UpdateEffectComponent;

const useUpdateEffect = (effect, deps) => {
  function useFirstMountState() {
    const isFirst = useRef(true);

    if (isFirst.current) {
      isFirst.current = false;

      return true;
    }

    return isFirst.current;
  }
  const isFirstMount = useFirstMountState();

  useEffect(() => {
    if (!isFirstMount) {
      return effect();
    }
  }, deps);
};
