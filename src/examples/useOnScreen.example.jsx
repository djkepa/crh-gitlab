import React from 'react';
import { useOnScreen } from '@custom-react-hooks/all';

const OnScreenComponent = () => {
  const { ref, isIntersecting } = useOnScreen({ threshold: 1 }, false);

  const style = {
    height: '100px',
    width: '100%',
    backgroundColor: isIntersecting ? '#1dd0d3' : '#ff896f',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    margin: '50px 0',
    transition: 'background-color 0.3s ease',
    borderRadius: '10px',
    padding: '10px',
  };

  return (
    <div
      ref={ref}
      style={style}
    >
      {isIntersecting ? 'Element is visible!' : 'Scroll down...'}
    </div>
  );
};

export default OnScreenComponent;
