import React, { useEffect, useState, useRef } from 'react';
import { useScript } from '@custom-react-hooks/all';
import { useFrame } from 'react-frame-component';

const ScriptComponent = () => {
  const { document, window } = useFrame();
  const status = useScript(
    'https://cdn.jsdelivr.net/npm/canvas-confetti@1.4.0/dist/confetti.browser.js',
  );

  const triggerConfetti = () => {
    if (status === 'ready' && window.confetti) {
      window.confetti({
        particleCount: 100,
        spread: 70,
        origin: { y: 0.6 },
      });
    }
  };

  useEffect(() => {
    appendScriptToIframe();
  }, []);

  const appendScriptToIframe = () => {
    const iframeDoc = document;

    if (iframeDoc) {
      const script = iframeDoc.createElement('script');
      script.src = 'https://cdn.jsdelivr.net/npm/canvas-confetti@1.4.0/dist/confetti.browser.js';
      script.async = true;

      iframeDoc.head.appendChild(script);
    }
  };

  return (
    <div className="center">
      <h1>Confetti Script Loader</h1>

      <p>Script Loading Status: {status}</p>

      <button
        onClick={triggerConfetti}
        disabled={status !== 'ready'}
      >
        Trigger Confetti
      </button>
      {status === 'error' && (
        <p style={{ color: 'red' }}>Failed to load the script. Please check the URL.</p>
      )}
    </div>
  );
};

export default ScriptComponent;
