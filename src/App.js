import React from 'react';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import AsyncPage from './pages/AsyncPage';
import ClickOutsidePage from './pages/ClickOutsidePage';
import ClipboardPage from './pages/ClipboardPage';
import DebouncePage from './pages/DebouncePage';
import DocumentTitlePage from './pages/DocumentTitlePage';
import DragDropPage from './pages/DragDropPage';
import EffectOncePage from './pages/EffectOncePage';
import ElementSizePage from './pages/ElementSizePage';
import EventListenerPage from './pages/EventListenerPage';
import FetchPage from './pages/FetchPage';
import FormPage from './pages/FormPage';
import GeoLocationPage from './pages/GeoLocationPage';
import HoverPage from './pages/HoverPage';
import IdlePage from './pages/IdlePage';
import ImageLoadPage from './pages/ImageLoadPage';
import KeyPressPage from './pages/KeyPressPage';
import LockBodyScrollPage from './pages/LockBodyScrollPage';
import LongPressPage from './pages/LongPressPage';
import MediaDevicesPage from './pages/MediaDevicesPage';
import MediaQueryPage from './pages/MediaQueryPage';
import MousePage from './pages/MousePage';
import OnScreenPage from './pages/OnScreenPage';
import OrientationPage from './pages/OrientationPage';
import PermissionPage from './pages/PermissionPage';
import PortalPage from './pages/PortalPage';
import ScriptPage from './pages/ScriptPage';
import StatusPage from './pages/StatusPage';
import StepPage from './pages/StepPage';
import StoragePage from './pages/StoragePage';
import ThrottlePage from './pages/ThrottlePage';
import TimeoutPage from './pages/TimeoutPage';
import TogglePage from './pages/TogglePage';
import UpdateEffectPage from './pages/UpdateEffectPage';
import WindowSizePage from './pages/WindowSizePage';

import './styles/main.scss';
import Main from './main';
import Layout from './components/layout';
import HomeLayout from './components/homeLayout';

function App() {
  return (
    <Router>
      <Routes>
        <Route
          path="/"
          element={
            <HomeLayout>
              <Main />
            </HomeLayout>
          }
        />
        <Route
          path="/use-async"
          element={
            <Layout>
              <AsyncPage />
            </Layout>
          }
        />
        <Route
          path="/use-click-outside"
          element={
            <Layout>
              <ClickOutsidePage />
            </Layout>
          }
        />
        <Route
          path="/use-clipboard"
          element={
            <Layout>
              <ClipboardPage />
            </Layout>
          }
        />
        <Route
          path="/use-debounce"
          element={
            <Layout>
              <DebouncePage />
            </Layout>
          }
        />
        <Route
          path="/use-document-title"
          element={
            <Layout>
              <DocumentTitlePage />
            </Layout>
          }
        />
        <Route
          path="/use-drag-drop"
          element={
            <Layout>
              <DragDropPage />
            </Layout>
          }
        />
        <Route
          path="/use-effect-once"
          element={
            <Layout>
              <EffectOncePage />
            </Layout>
          }
        />
        <Route
          path="/use-element-size"
          element={
            <Layout>
              <ElementSizePage />
            </Layout>
          }
        />
        <Route
          path="/use-event-listener"
          element={
            <Layout>
              <EventListenerPage />
            </Layout>
          }
        />
        <Route
          path="/use-fetch"
          element={
            <Layout>
              <FetchPage />
            </Layout>
          }
        />
        <Route
          path="/use-form"
          element={
            <Layout>
              <FormPage />
            </Layout>
          }
        />
        <Route
          path="/use-geo-location"
          element={
            <Layout>
              <GeoLocationPage />
            </Layout>
          }
        />
        <Route
          path="/use-hover"
          element={
            <Layout>
              <HoverPage />
            </Layout>
          }
        />
        <Route
          path="/use-idle"
          element={
            <Layout>
              <IdlePage />
            </Layout>
          }
        />
        <Route
          path="/use-image-load"
          element={
            <Layout>
              <ImageLoadPage />
            </Layout>
          }
        />
        <Route
          path="/use-key-press"
          element={
            <Layout>
              <KeyPressPage />
            </Layout>
          }
        />
        <Route
          path="/use-lock-body-scroll"
          element={
            <Layout>
              <LockBodyScrollPage />
            </Layout>
          }
        />
        <Route
          path="/use-long-press"
          element={
            <Layout>
              <LongPressPage />
            </Layout>
          }
        />
        <Route
          path="/use-media-devices"
          element={
            <Layout>
              <MediaDevicesPage />
            </Layout>
          }
        />
        <Route
          path="/use-media-query"
          element={
            <Layout>
              <MediaQueryPage />
            </Layout>
          }
        />
        <Route
          path="/use-mouse"
          element={
            <Layout>
              <MousePage />
            </Layout>
          }
        />
        <Route
          path="/use-on-screen"
          element={
            <Layout>
              <OnScreenPage />
            </Layout>
          }
        />
        <Route
          path="/use-orientation"
          element={
            <Layout>
              <OrientationPage />
            </Layout>
          }
        />
        <Route
          path="/use-permission"
          element={
            <Layout>
              <PermissionPage />
            </Layout>
          }
        />
        <Route
          path="/use-portal"
          element={
            <Layout>
              <PortalPage />
            </Layout>
          }
        />
        <Route
          path="/use-script"
          element={
            <Layout>
              <ScriptPage />
            </Layout>
          }
        />
        <Route
          path="/use-status"
          element={
            <Layout>
              <StatusPage />
            </Layout>
          }
        />
        <Route
          path="/use-step"
          element={
            <Layout>
              <StepPage />
            </Layout>
          }
        />
        <Route
          path="/use-storage"
          element={
            <Layout>
              <StoragePage />
            </Layout>
          }
        />
        <Route
          path="/use-throttle"
          element={
            <Layout>
              <ThrottlePage />
            </Layout>
          }
        />
        <Route
          path="/use-timeout"
          element={
            <Layout>
              <TimeoutPage />
            </Layout>
          }
        />
        <Route
          path="/use-toggle"
          element={
            <Layout>
              <TogglePage />
            </Layout>
          }
        />
        <Route
          path="/use-update-effect"
          element={
            <Layout>
              <UpdateEffectPage />
            </Layout>
          }
        />
        <Route
          path="/use-window-size"
          element={
            <Layout>
              <WindowSizePage />
            </Layout>
          }
        />
      </Routes>
    </Router>
  );
}

export default App;
