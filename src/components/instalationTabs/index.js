import React, { useCallback, useState } from 'react';
import { ClipboardIcon } from '@heroicons/react/24/solid';
import { vscDarkPlus } from 'react-syntax-highlighter/dist/esm/styles/prism';
import SyntaxHighlighter from 'react-syntax-highlighter/dist/esm/prism-async';

import './instalationTabs.scss';

const InstallationTabs = ({ commands }) => {
  const [activeTab, setActiveTab] = useState(0);
  const { copyToClipboard } = useClipboard();

  const handleCopyClick = useCallback(
    (commandIndex) => {
      const command = commands[commandIndex];
      copyToClipboard(command);
    },
    [commands, copyToClipboard],
  );

  const tabTitles = ['NPM', 'Yarn'];

  return (
    <div>
      <div className="installation-tabs">
        {tabTitles.map((title, index) => (
          <button
            key={title}
            className={`tab-button ${activeTab === index ? 'active' : ''}`}
            onClick={() => setActiveTab(index)}
          >
            {title}
          </button>
        ))}
      </div>

      <div className="tab-content">
        <div className="command">
          <SyntaxHighlighter
            language="bash"
            style={vscDarkPlus}
          >
            {commands[activeTab]}
          </SyntaxHighlighter>
          <button
            className="command-btn"
            onClick={() => handleCopyClick(activeTab)}
          >
            <svg
              fill="#fff"
              width="800px"
              height="800px"
              viewBox="0 0 32 32"
              data-name="Layer 1"
              id="Layer_1"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path d="M27.2,8.22H23.78V5.42A3.42,3.42,0,0,0,20.36,2H5.42A3.42,3.42,0,0,0,2,5.42V20.36a3.43,3.43,0,0,0,3.42,3.42h2.8V27.2A2.81,2.81,0,0,0,11,30H27.2A2.81,2.81,0,0,0,30,27.2V11A2.81,2.81,0,0,0,27.2,8.22ZM5.42,21.91a1.55,1.55,0,0,1-1.55-1.55V5.42A1.54,1.54,0,0,1,5.42,3.87H20.36a1.55,1.55,0,0,1,1.55,1.55v2.8H11A2.81,2.81,0,0,0,8.22,11V21.91ZM28.13,27.2a.93.93,0,0,1-.93.93H11a.93.93,0,0,1-.93-.93V11a.93.93,0,0,1,.93-.93H27.2a.93.93,0,0,1,.93.93Z" />
            </svg>
          </button>
        </div>
      </div>
    </div>
  );
};

export default InstallationTabs;

const isClipboardAvailable =
  typeof navigator !== 'undefined' && typeof navigator.clipboard !== 'undefined';

function useClipboard() {
  const [state, setState] = useState({ success: false, error: null });

  const copyToClipboard = useCallback(async (text) => {
    if (!isClipboardAvailable) {
      setState({ success: false, error: 'Clipboard is not available' });
      return;
    }

    if (!text.trim()) {
      setState({ success: false, error: 'Cannot copy empty or whitespace text' });
      return;
    }

    try {
      await navigator.clipboard.writeText(text);
      setState({ success: true, error: null });
    } catch (error) {
      setState({ success: false, error: 'Failed to copy' });
    }
  }, []);

  const pasteFromClipboard = useCallback(async () => {
    if (!isClipboardAvailable) {
      setState({ success: false, error: 'Clipboard is not available' });
      return;
    }

    try {
      const text = await navigator.clipboard.readText();
      if (!text.trim()) {
        return '';
      }
      setState({ success: true, error: null });
      return text;
    } catch (error) {
      setState({ success: false, error: 'Failed to paste' });
      return '';
    }
  }, []);

  return { copyToClipboard, pasteFromClipboard, state };
}
