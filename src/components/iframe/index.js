import React, { useRef, useEffect } from 'react';
import { createRoot } from 'react-dom/client';

const Iframe = ({ children, className, hookName }) => {
  const iframeRef = useRef(null);

  const appendScriptToIframe = () => {
    const iframeDoc = document;

    if (iframeDoc) {
      const script = iframeDoc.createElement('script');
      script.src = 'https://cdn.jsdelivr.net/npm/canvas-confetti@1.4.0/dist/confetti.browser.js';
      script.async = true;

      iframeDoc.head.appendChild(script);
    }
  };

  useEffect(() => {
    const iframe = iframeRef.current;
    const iframeDocument = iframe.contentDocument || iframe.contentWindow.document;

    const reactAppRoot = iframeDocument.createElement('div');
    reactAppRoot.id = 'react-iframe-root';
    iframeDocument.body.appendChild(reactAppRoot);

    const root = createRoot(reactAppRoot);
    root.render(children);

    return () => {
      // Defer the unmount operation to the next event loop cycle
      setTimeout(() => {
        root.unmount();
        if (iframeDocument.body.contains(reactAppRoot)) {
          iframeDocument.body.removeChild(reactAppRoot);
        }
      }, 0);
    };
  }, [children]);

  return (
    <iframe
      ref={iframeRef}
      className={className}
      title="Iframe Component"
      width={500}
      height={500}
    />
  );
};

export default Iframe;
