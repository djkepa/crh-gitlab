import { useState, useEffect, useCallback } from 'react';

export function useWindowSize(debounceDelay = 100, frameWindow = window, frameDocument = document) {
  const getSize = () => ({
    width: frameWindow ? frameWindow.innerWidth : 0,
    height: frameWindow ? frameWindow.innerHeight : 0,
  });

  const [windowSize, setWindowSize] = useState(getSize);

  const debounce = (fn, ms) => {
    let timer = null;
    return () => {
      clearTimeout(timer);
      timer = setTimeout(() => {
        timer = null;
        fn();
      }, ms);
    };
  };

  const handleResize = useCallback(() => {
    const handleResizeDebounced = debounce(() => {
      setWindowSize(getSize());
    }, debounceDelay);
    handleResizeDebounced();
  }, [debounceDelay]);

  useEffect(() => {
    if (frameWindow && frameDocument) {
      frameWindow.addEventListener('resize', handleResize);
      return () => frameWindow.removeEventListener('resize', handleResize);
    }
  }, [handleResize, frameWindow, frameDocument]);

  return windowSize;
}
