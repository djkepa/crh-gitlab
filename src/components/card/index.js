import React from 'react';

import './card.scss';

const Card = ({ title, description, url }) => {
  return (
    <a
      href={url}
      class="card"
    >
      <div class="card2">{title}</div>
    </a>
  );
};

export default Card;
