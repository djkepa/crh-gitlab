import { useState, useCallback } from 'react';

const useDragDrop = (items, setItems, onDrop) => {
  const [state, setState] = useState({
    isDragging: false,
    isOver: false,
    dragData: null,
    dropData: null,
  });

  const handleDragStart = useCallback((e, data) => {
    e.dataTransfer.setData('application/reactflow', JSON.stringify(data));
    e.dataTransfer.effectAllowed = 'move';
    setState((s) => ({ ...s, isDragging: true, dragData: data }));
  }, []);

  const handleDragEnd = useCallback(() => {
    setState((s) => ({ ...s, isDragging: false, dragData: null }));
  }, []);

  const handleDragOver = useCallback((e) => {
    e.preventDefault();
    setState((s) => ({ ...s, isOver: true }));
  }, []);

  const handleDrop = useCallback(
    (e) => {
      e.preventDefault();
      const sourceData = JSON.parse(e.dataTransfer.getData('application/reactflow'));
      const sourceIndex = items.findIndex((item) => item.id === sourceData.id);
      if (sourceIndex < 0) {
        return;
      }
      setItems((prevItems) => {
        // Logic to reorder items or handle drop
        return prevItems; // Return the updated items array
      });
      setState((s) => ({ ...s, isOver: false, dropData: sourceData }));
      if (onDrop) {
        onDrop(sourceData);
      }
    },
    [items, onDrop],
  );

  const handleDragLeave = useCallback(() => {
    setState((s) => ({ ...s, isOver: false }));
  }, []);

  return {
    state,
    bindDrag: {
      draggable: true,
      onDragStart: handleDragStart,
      onDragEnd: handleDragEnd,
    },
    bindDrop: {
      onDragOver: handleDragOver,
      onDrop: handleDrop,
      onDragLeave: handleDragLeave,
    },
  };
};

export default useDragDrop;
