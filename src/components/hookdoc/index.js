import React, { useEffect, useState } from 'react';
import { Prism as SyntaxHighlighter } from 'react-syntax-highlighter';
import { vscDarkPlus } from 'react-syntax-highlighter/dist/esm/styles/prism';
import './HookDoc.scss';
import Frame, { useFrame } from 'react-frame-component';
import InstallationTabs from '../instalationTabs';

const HookDoc = ({ data, children, hookName }) => {
  const { name, description, features, installation, usage, apiReference, contributing, useCases } =
    data;
  const handleCopy = (command) => {
    navigator.clipboard.writeText(command);
  };

  const iframeCSS = `

  body {
    font-family: 'Poppins';
    color: white;
  }

  h3 {
    font-family: 'Poppins';
    font-weight: 600;
    font-size: 36px;
    line-height: 42px;
    color: white;
  }

  input {
    background-color: #21212d;
    padding: 0 10px;
    border: 2px solid white;
    height: 44px;
    border-radius: 4px;
    color: white;
    font-family: 'Poppins';
    font-weight: 600;
  }

  select {
    background-color: #21212d;
    padding: 0 10px;
    border: 2px solid white;
    height: 44px;
    border-radius: 4px;
    color: white;
    font-family: 'Poppins';
    font-weight: 600;
  }

  select:focus {
    outline: #ff896f;
    border-color: #ff896f;
  }

  option {
    padding: 10px;
  }

  input::placeholder {
    color: gray;
    font-family: 'Poppins';
    font-weight: 400;
  }

  input:focus {
    outline: #3860e8;
    border-color: #3860e8;
  }

  button {
    height: 44px;
    padding: 10px 20px;
    color: white;
    font-family: 'Poppins';
    font-weight: 600;
    background-color: #4f46e5;
    border-radius: 4px;
    outline: 0;
    border: 2px solid #4f46e5;
    cursor: pointer
  }

  button:hover {
    color: #4f46e5;
    background-color: white;
  }

  .async input {
    width: 460px;
    margin-right: 10px;

     @media (max-width: 767px) {
       width: 100%;
    }
  }

  .async .result {
    margin-top: 15px;
  }

  p,
  label {
    font-family: 'Poppins';
    font-weight: 500;
    color: white;
  }

  body {
    display: flex;
    align-items: center;
    justify-content: center;
  }

  .center {
    display: flex;
    align-items: center;
    justify-content: center;
    flex-direction: column;
  }

  .async {}

  .async input[type="input"] {
    width: 360px;
    margin-right: 20px;
  }

  .async label {
    display: flex;
    align-item: center;
    margin-top: 10px;
  }

  .async label input {
    width: 20px;
    height: 20px;
  }

  .modal {
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    top: 80px;
    left: 0;
    right: 0;
    margin: auto;
    width: 300px;
    height: 100px;
    border: 3px dotted #18181d;
    border-radius: 14px;
    background: white;
    padding: 20px;
    position: absolute;
    color: #18181d;
  }

  .modal p {
    color: #21212d;
  }

  .clipboard {
    display: flex;
    flex-direction: column;
    gap: 10px;

     
  }

  .clipboard input {
    width: 400px;

    @media (max-width: 767px) {
       width: 100%;
    }
  }


  .clipboard input:focus {
    outline: #ff896f;
    border-color: #ff896f;
  }

  .clipboard button {
    background-color: #00989a;
    border-color: #00989a;
  }

  .clipboard button:hover {
    background-color: transparent;
    color: #00989a;
  }

  .debounce input {
    width: 300px;
    outline: #ff589c;
    border-color: #ff589c;
  }

  .debounce span {
    color: #ff589c;
  }

  .document-title {
    display: flex;
    flex-direction: column;
    align-items: center;
    gap: 10px;
  }

  .element-size .resize {
    resize: both;
    overflow: auto;
    width: 200px;
    height: 100px;
    border: 2px dotted #f9f8ff;
    padding: 10px;
    border-radius: 5px;
  }

  .resize {
    border: 2px solid #007bff;
    padding: 20px;
    margin: 10px 0;
  }

  .form .input-field {
    display: flex;
    flex-direction: column;
    align-items: baseline;
    margin-bottom: 20px;
    position: relative;
  }

  .form .input-field input {
    width: 260px;
  }

  .form .error {
    position: absolute;
    bottom: -20px;
    left: 0;
    font-family: 'Poppins';
    font-size: 14px;
    color: white;
    font-weight: 500;
    color: #ff5868;
  }

  .form .pair {
    display: flex;
    justify-content: space-between;
    margin-top: 36px;
  }

  .lock-body-scroll {
    width: 320px;
  }

  .lock-body-scroll .modal {
    margin-top:45px;
  }

  .red-btn {
    background-color: lightcoral;
    border-color: lightcoral;
    margin-left: 10px;
  }

  .red-btn:hover {
    background-color: white;
    color: lightcoral;
    border-color: lightcoral;
  }

  .btns {
    display: flex;
    gap: 20px;
    margin-bottom: 20px;
  }

  .btns button {
    min-width: 120px;
  }

  .item-between {
    display: flex;
    justify-content: space-between;

    @media (max-width: 767px) {
       flex-direction:column;
    }
  }

  .keyboard-key {
    background-color: #f0f0f0;
    border: 1px solid #d0d0d0;
    border-radius: 4px;
    padding: 10px 20px;
    font-size: 16px;
    cursor: pointer;
    box-shadow: 0 4px 2px rgba(255, 255, 255, 0.3),
      0 2px 6px rgba(255, 255, 255, 0.2);
    transition: all 0.1s ease;
    outline: none;
    user-select: none;
    color: rgba(0, 0, 0, 0.7)
  }

  .keyboard-key:active,
  .keyboard-key.pressed {
    box-shadow: 0 2px 3px rgba(0, 0, 0, 0.1);
    transform: translateY(2px);
  }

  .btn {
    height: 44px;
    padding: 10px 20px;
    color: white;
    font-family: 'Poppins';
    font-weight: 600;
    border-radius: 4px;
    outline: 0;
    cursor: pointer
  }

  .btn-blue {
    background-color: #3860e8;
    border: 2px solid #3860e8;
  }

  .btn-blue:hover {
    color: #18181d;
    background: linear-gradient(45deg, rgba(56,96,232,1) 0%, rgba(255,255,255,1) 100%);
  }

  .btn-red {
    background-color: #ff5868;
    border: 2px solid #ff5868;
  }

  .btn-red:hover {
    color: #18181d;
    border: 2px solid #ff5868;
    background: linear-gradient(45deg, #ff5868 0%, rgba(255,255,255,1) 100%);
  }

  .btn-green {
    background-color: #1dd0d3;
    border: 2px solid #1dd0d3;
  }

  .btn-green:hover {
    color: #18181d;
    border: 2px solid #1dd0d3;
    background: linear-gradient(45deg, #1dd0d3 0%, rgba(255,255,255,1) 100%);
  }

  .btn-yellow {
    background-color: #f9f871;
    border: 2px solid #f9f871;
  }

  .btn-yellow:hover {
    color: #18181d;
    border: 2px solid #f9f871;
    background: linear-gradient(45deg, #f9f871 0%, rgba(255,255,255,1) 100%);
  }

  .c-red {
  color: #ff5868 !important;
  }

  .c-gray {
    color: #f9f8ff !important;
  }

  .c-blue {
    color: #3860e8 !important;
  }

  .c-green {
    color: #1dd0d3 !important;
  }

  .c-yellow {
    color: #f9f871 !important;
  }

  .c-orange {
    color: #ff896f !important;
  }

  .c-pink {
    color: #ff589c !important;
  }

  .toggle .btns {
     @media (max-width: 767px) {
      flex-direction: column;
      align-items: center;
      margin: auto;
    }
  }

  .toggle .btns {
     @media (max-width: 767px) {
       width: max-content;
    }
  }

  .storage {
    display: flex;
    gap: 80px;
    align-items: flex-start;

    @media (max-width: 767px) {
      flex-direction: column;
      align-items: center;
    }
  }

  .storage-item {
    display: flex;
    align-items: center;
    gap:10px;
  }

  .storage-item svg {
    width:30px;
    background:transparent;
    fill:#f9f8ff;
    stroke: #3860e8;
    cursor:pointer;
  }

  `;

  const resize = hookName === 'media-query' ? 'auto' : 'none';

  function boldQuotedText(inputString) {
    const regex = /(['`])(.*?)\1/g;

    return inputString.replace(regex, (match, quote, text) => `<strong>${text}</strong>`);
  }

  const SafeHtmlComponent = ({ html }) => <span dangerouslySetInnerHTML={{ __html: html }} />;

  const transformDesc = boldQuotedText(description);

  return (
    <div className="hook-doc container">
      <div className="hook-doc__glass"></div>

      <div className="hook-doc__logo">
        <a href="/">
          <img
            src="https://i.ibb.co/ykSxVSX/custom-react-hooks-logo.png"
            alt="Custom react hooks library logo"
          />
        </a>
      </div>

      <h1 className="hook-doc__title">{name}</h1>
      <div className="hook-doc__description">
        <h5>DESCRIPTION</h5>
        <p>
          <SafeHtmlComponent html={transformDesc} />
        </p>
      </div>
      <div className="hook-doc__installation">
        <h5>INSTALL</h5>
        <InstallationTabs commands={installation.currentHook} />
        <br />
        <InstallationTabs commands={installation.allHooks} />
      </div>
      <h5>FEATURES</h5>
      <ul className="hook-doc__list">
        {features &&
          features.map((feature, index) => (
            <li key={index}>
              <p>
                <b>{feature.name}: </b>
                <SafeHtmlComponent html={boldQuotedText(feature.description)} />
              </p>
            </li>
          ))}
      </ul>
      <div className="hook-doc__api">
        {apiReference.parameters && apiReference.parameters.length !== 0 && (
          <>
            <h5>PARAMETERS</h5>
            <table className="hook-doc__table">
              <tbody>
                <tr>
                  <th>Name</th>
                  <th>Type</th>
                  <th>Description</th>
                </tr>
                {apiReference.parameters &&
                  apiReference.parameters.map((param, index) => (
                    <tr key={index}>
                      <td>{param.name}</td>
                      <td>{param.type}</td>
                      <td>
                        <SafeHtmlComponent html={boldQuotedText(param.description)} />
                      </td>
                    </tr>
                  ))}
              </tbody>
            </table>
          </>
        )}
        {Array.isArray(apiReference.returns) && apiReference.returns.length !== 0 && (
          <>
            <h5>RETURNS</h5>
            <table className="hook-doc__table">
              <tbody>
                <tr>
                  <th>Name</th>
                  <th>Type</th>
                  <th>Description</th>
                </tr>
                {apiReference.returns &&
                  apiReference.returns.map((retVal, index) => (
                    <tr key={index}>
                      <td>{retVal.name}</td>
                      <td>{retVal.type}</td>
                      <td>
                        <SafeHtmlComponent html={boldQuotedText(retVal.description)} />
                      </td>
                    </tr>
                  ))}
              </tbody>
            </table>
          </>
        )}
      </div>
      <div className="hook-doc__usage">
        <h5>USAGE</h5>
        <SyntaxHighlighter
          language="javascript"
          style={vscDarkPlus}
        >
          {usage.code}
        </SyntaxHighlighter>
      </div>
      <div className="hook-doc__example">
        <h5>DEMO</h5>

        <Frame
          style={{ backgroundColor: '#18181d', padding: '32px', resize: resize }}
          head={
            <>
              <style>{iframeCSS}</style>
              <link
                href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap"
                rel="stylesheet"
              />
            </>
          }
        >
          {children}
        </Frame>
      </div>
      <h5>USE CASES</h5>
      <ul className="hook-doc__list">
        {useCases &&
          useCases.map((cases, index) => (
            <li key={index}>
              <p>
                <b>{cases.name}: </b>
                <SafeHtmlComponent html={boldQuotedText(cases.description)} />
              </p>
            </li>
          ))}
      </ul>
    </div>
  );
};

export default HookDoc;
