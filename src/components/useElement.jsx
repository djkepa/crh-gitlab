import { useCallback, useEffect, useState } from 'react';

export function useElementSize() {
  const [ref, setRef] = useState(null);
  const [size, setSize] = useState({ width: 0, height: 0 });

  const handleSize = useCallback(() => {
    if (ref) {
      setSize({
        width: ref.offsetWidth,
        height: ref.offsetHeight,
      });
    }
  }, [ref]);

  useEffect(() => {
    if (!ref) return;

    handleSize();

    const resizeObserver = new ResizeObserver(handleSize);
    resizeObserver.observe(ref);

    return () => resizeObserver.disconnect();
  }, [ref, handleSize]);

  return [setRef, size];
}
