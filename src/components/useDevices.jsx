import { useState, useEffect } from 'react';

const useMediaDevices = (requestPermission = false) => {
  const [state, setState] = useState({
    devices: [],
    isLoading: false,
    error: null,
  });

  useEffect(() => {
    if (typeof navigator === 'undefined' || typeof navigator.mediaDevices === 'undefined') {
      setState((s) => ({ ...s, error: 'Media devices are not available' }));
      return;
    }

    const handleDeviceChange = async () => {
      try {
        if (requestPermission) {
          await navigator.mediaDevices.getUserMedia({ audio: true, video: true });
        }

        const devices = await navigator.mediaDevices.enumerateDevices();
        setState({
          devices: devices.map((device) => ({
            id: device.deviceId,
            kind: device.kind,
            label: device.label || 'Unknown Device',
          })),
          isLoading: false,
          error: null,
        });
      } catch (error) {
        setState((s) => ({ ...s, isLoading: false, error: 'Unable to enumerate devices' }));
      }
    };

    setState((s) => ({ ...s, isLoading: true }));
    handleDeviceChange();

    navigator.mediaDevices.addEventListener('devicechange', handleDeviceChange);

    return () => {
      navigator.mediaDevices.removeEventListener('devicechange', handleDeviceChange);
    };
  }, [requestPermission]);

  return state;
};

export default useMediaDevices;
