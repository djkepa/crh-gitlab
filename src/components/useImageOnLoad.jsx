import { useState, useEffect, useCallback } from 'react';

function useImageLoad({ thumbnailSrc, fullSrc, lazyLoad = false }, imgRef) {
  const [state, setState] = useState({
    src: lazyLoad ? '' : thumbnailSrc, // Start with thumbnailSrc if not lazy loading
    isLoading: !lazyLoad,
    isLoaded: false,
    hasError: false,
  });

  const loadImage = useCallback(
    (imageSrc: string) => {
      if (!imgRef.current) return;

      setState((prevState) => ({ ...prevState, isLoading: true })); // Start loading

      const img = new Image();
      img.src = imageSrc;

      img.onload = () => {
        setState({
          src: imageSrc,
          isLoading: false,
          isLoaded: true,
          hasError: false,
        });
        // If we just loaded the thumbnail, and there is a fullSrc, start loading it
        if (imageSrc === thumbnailSrc && fullSrc) {
          loadImage(fullSrc);
        }
      };

      img.onerror = () => {
        setState((prevState) => ({ ...prevState, isLoading: false, hasError: true }));
      };
    },
    [thumbnailSrc, fullSrc, imgRef],
  );

  useEffect(() => {
    if (typeof window === 'undefined') return;
    // If not lazy loading, load the thumbnail immediately
    if (!lazyLoad) {
      loadImage(thumbnailSrc);
    } else if (imgRef.current) {
      // If lazy loading, set up IntersectionObserver
      const observer = new IntersectionObserver(
        (entries) => {
          entries.forEach((entry) => {
            if (entry.isIntersecting) {
              loadImage(thumbnailSrc);
              observer.unobserve(entry.target);
            }
          });
        },
        { threshold: 0.1 },
      );

      observer.observe(imgRef.current);

      // Clean up the observer on unmount
      return () => observer.disconnect();
    }
  }, [thumbnailSrc, lazyLoad, loadImage, imgRef]);

  return state;
}

export default useImageLoad;
