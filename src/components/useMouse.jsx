import { useState, useEffect, useCallback, useRef } from 'react';

function useMouse(ref, options = { offsetX: 10, offsetY: 10, avoidEdges: false }) {
  const [mousePosition, setMousePosition] = useState({ x: 0, y: 0 });
  const frameId = useRef(null);

  const updateMousePosition = useCallback(
    (ev) => {
      if (frameId.current !== null) {
        cancelAnimationFrame(frameId.current);
      }

      frameId.current = requestAnimationFrame(() => {
        let newX = ev.clientX + options.offsetX;
        let newY = ev.clientY + options.offsetY;

        if (options.avoidEdges) {
          const screenWidth = window.innerWidth;
          const screenHeight = window.innerHeight;
          const tooltipWidth = options.tooltipWidth || 100;
          const tooltipHeight = options.tooltipHeight || 50;

          if (newX + tooltipWidth > screenWidth) {
            newX = ev.clientX - tooltipWidth;
          }

          if (newY + tooltipHeight > screenHeight) {
            newY = ev.clientY - tooltipHeight;
          }
        }

        setMousePosition({ x: newX, y: newY });
      });
    },
    [options],
  );

  useEffect(() => {
    const isInsideIframe = window.location !== window.parent.location;
    const targetDocument = isInsideIframe ? window.document : document;

    if (!ref.current) {
      return;
    }

    const handleMouseMove = (ev) => {
      if (ref.current && ref.current.contains(ev.target)) {
        updateMousePosition(ev);
      }
    };

    targetDocument.addEventListener('mousemove', handleMouseMove);

    return () => {
      targetDocument.removeEventListener('mousemove', handleMouseMove);
      if (frameId.current !== null) {
        cancelAnimationFrame(frameId.current);
      }
    };
  }, [ref, updateMousePosition]);

  return mousePosition;
}

export default useMouse;
