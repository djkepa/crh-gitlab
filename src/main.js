import React from 'react';
import InstallationTabs from './components/instalationTabs';

import data from './data/all.json';
import Card from './components/card';

function Main() {
  const installData = ['npm install @custom-react-hooks/all', 'yarn add @custom-react-hooks/all'];
  return (
    <div>
      <div class="comet-rain">
        <svg
          aria-hidden="true"
          id="svg-comet"
          class="svg-defs"
          data-name="comet"
          viewBox="0 0 195 173.05"
          width="195"
          height="173.05"
        >
          <symbol id="comet">
            <path d="M192.39.66c-33.66,31.46-93.3,81.16-129,108.88L3.42,156c-5,3.89-4.29,11.28.74,15.4s12.84-.83,13.1-1.07C51.09,138.62,82.6,104.72,116.64,73c8.89-8.29,66.23-59.5,77.47-70.13C196.17,1,194.24-1.08,192.39.66Z" />
          </symbol>
        </svg>

        <svg
          aria-hidden="true"
          width="0"
          height="0"
        >
          <defs>
            <linearGradient
              id="gradient"
              x1="194.94"
              y1="4.2"
              x2="11.72"
              y2="161.98"
              gradientUnits="userSpaceOnUse"
            >
              <stop
                offset="0%"
                stop-color="#fff"
                stop-opacity="0.03"
              ></stop>
              <stop
                offset="100%"
                stop-color="var(--t-sun-color)"
              ></stop>
            </linearGradient>
          </defs>
        </svg>

        <svg
          aria-hidden="true"
          class="comet-01 comet-instance"
          width="195"
          height="173.05"
          viewBox="0 0 195 173.05"
        >
          <use xlinkHref="#comet" />
        </svg>

        <svg
          aria-hidden="true"
          class="comet-02 comet-instance"
          width="195"
          height="173.05"
          viewBox="0 0 195 173.05"
        >
          <use xlinkHref="#comet" />
        </svg>

        <svg
          aria-hidden="true"
          role="presentation"
          class="comet-03 comet-instance"
          width="195"
          height="173.05"
          viewBox="0 0 195 173.05"
        >
          <use xlinkHref="#comet" />
        </svg>

        <svg
          aria-hidden="true"
          class="comet-04 comet-instance"
          width="195"
          height="173.05"
          viewBox="0 0 195 173.05"
        >
          <use xlinkHref="#comet" />
        </svg>

        <svg
          aria-hidden="true"
          class="comet-05 comet-instance"
          width="195"
          height="173.05"
          viewBox="0 0 195 173.05"
        >
          <use xlinkHref="#comet" />
        </svg>

        <svg
          aria-hidden="true"
          class="comet-06 comet-instance"
          width="195"
          height="173.05"
          viewBox="0 0 195 173.05"
        >
          <use xlinkHref="#comet" />
        </svg>
      </div>
      <div className="logo">
        <img
          src="https://i.ibb.co/ykSxVSX/custom-react-hooks-logo.png"
          alt="Custom react hooks library logo"
        />
      </div>
      <div className="installations">
        <InstallationTabs commands={installData} />
      </div>
      <div className="container">
        {/* <div className="hook-doc__glass"></div> */}
        <div className="card-block">
          {data.length !== 0 &&
            data.map((item, index) => {
              return (
                <Card
                  key={index}
                  title={item.name}
                  url={item.url}
                />
              );
            })}
        </div>
      </div>
    </div>
  );
}

export default Main;
