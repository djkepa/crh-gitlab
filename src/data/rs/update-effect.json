{
  "name": "useUpdateEffect Hook",
  "description": "The `useUpdateEffect` hook is an enhanced version of React's `useEffect` that is triggered only when dependencies update, skipping the effect on the component's initial mount. It's particularly useful for effects that need to run in response to specific changes after the initial rendering.",
  "installation": {
    "currentHook": [
      "npm install @custom-react-hooks/use-update-effect",
      "yarn add @custom-react-hooks/use-update-effect"
    ],
    "allHooks": ["npm install @custom-react-hooks/all", "yarn add @custom-react-hooks/all"]
  },
  "features": [
    {
      "name": "Skips Initial Render",
      "description": "Executes the effect only on updates, not during the initial component mount."
    },
    {
      "name": "Custom Cleanup Function",
      "description": "Similar to `useEffect`, it allows for a cleanup function to be returned from the effect."
    },
    {
      "name": "Compatible with SSR",
      "description": "Designed to work seamlessly in server-side rendering environments."
    }
  ],
  "usage": {
    "code": "import React, { useState } from 'react';\nimport { useUpdateEffect } from '@custom-react-hooks/all';\n\nconst UpdateEffectComponent = () => {\n  const [count, setCount] = useState(0);\n  const [message, setMessage] = useState('');\n\n  useUpdateEffect(() => {\n    setMessage(`Effect ran at count: ${count}`);\n  }, [count]);\n\n  return (\n    <div>\n      <button onClick={() => setCount(c => c + 1)}>Increment</button>\n      <p>Count: {count}</p>\n      <p>{message}</p>\n    </div>\n  );\n};\n\nexport default UpdateEffectComponent;"
  },
  "apiReference": {
    "parameters": [
      {
        "name": "effect",
        "type": "Function",
        "description": "The effect function to execute upon updates."
      },
      {
        "name": "deps",
        "type": "DependencyList",
        "description": "An array of dependencies that, when changed, trigger the effect."
      }
    ]
  },
  "useCases": [
    {
      "name": "Conditional Execution",
      "description": "Run effects based on specific conditions or changes."
    },
    {
      "name": "Efficient Updates",
      "description": "Optimize component behavior by limiting effects to only necessary renders."
    }
  ],
  "contributing": "Contributions to `useUpdateEffect` are welcome. Please submit issues or pull requests to enhance its functionality or address any concerns."
}
