{
  "name": "useEffectOnce Hook",
  "description": "`useEffectOnce` is a custom hook in React designed to mimic the behavior of `componentDidMount` and `componentWillUnmount` lifecycle methods in class components. It's a modified version of `useEffect` that runs only once when the component mounts.",
  "features": [
    {
      "name": "Single Execution",
      "description": "The hook executes the provided effect function once upon the component's initial render."
    },
    {
      "name": "Cleanup Capability",
      "description": "It supports an optional cleanup function, returned from the effect, which is called when the component unmounts."
    },
    {
      "name": "SSR Compatibility",
      "description": "As an extension of `useEffect`, it is naturally compatible with server-side rendering environments."
    }
  ],
  "installation": {
    "currentHook": [
      "npm install @custom-react-hooks/use-effect-once",
      "yarn add @custom-react-hooks/use-effect-once"
    ],
    "allHooks": ["npm install @custom-react-hooks/all", "yarn add @custom-react-hooks/all"]
  },
  "usage": {
    "code": "import React from 'react';\nimport { useEffectOnce } from '@custom-react-hooks/all';\n\nconst EffectOnceComponent = () => {\n  const [fibonacciSequence, setFibonacciSequence] = useState([]);\n\n  const calculateFibonacci = (n) => {\n    let sequence = [0, 1];\n    for (let i = 2; i < n; i++) {\n      sequence[i] = sequence[i - 1] + sequence[i - 2];\n    }\n    return sequence.slice(0, n);\n  };\n\n  useEffectOnce(() => {\n    const sequence = calculateFibonacci(5);\n    setFibonacciSequence(sequence);\n  });\n\n  return (\n    <div>\n      <p>First {5} numbers in the Fibonacci sequence:</p>\n      <ul>\n        {fibonacciSequence.map((number, index) => (\n          <li key={index}>{number}</li>\n        ))}\n      </ul>\n    </div>\n  );\n};\n\nexport default EffectOnceComponent;"
  },
  "apiReference": {
    "parameters": [
      {
        "name": "effect",
        "type": "Function",
        "description": "A function that will be executed once when the component mounts. This function can optionally return a cleanup function, which will be executed when the component unmounts."
      }
    ]
  },
  "useCases": [
    {
      "name": "Initial Setup",
      "description": "Perform setup operations like fetching initial data or setting up listeners."
    },
    {
      "name": "One-time Calculations",
      "description": "Compute values needed only once during the component's lifecycle."
    },
    {
      "name": "Single API Calls",
      "description": "Make a single API call when a component is rendered for the first time."
    },
    {
      "name": "Non-Recurring Subscriptions",
      "description": "Subscribe to a service or event listener that should only be initialized once."
    }
  ],
  "contributing": "Contributions to enhance `useEffectOnce` are always welcome. Feel free to submit issues or pull requests to the repository for further improvements."
}
