{
  "name": "usePortal Hook",
  "description": "The `usePortal` hook facilitates the creation and management of portal components in React applications. Portals provide a first-class way to render children into a DOM node that exists outside the DOM hierarchy of the parent component. This hook is particularly useful for rendering modals, dropdowns, and tooltips.",
  "installation": {
    "currentHook": [
      "npm install @custom-react-hooks/use-portal",
      "yarn add @custom-react-hooks/use-portal"
    ],
    "allHooks": ["npm install @custom-react-hooks/all", "yarn add @custom-react-hooks/all"]
  },
  "features": [
    {
      "name": "Dynamic Portal Creation",
      "description": "Automatically creates and manages a DOM element for the portal."
    },
    {
      "name": "Simple State Management",
      "description": "Provides functions to open and close the portal, along with a state variable to track its visibility."
    },
    {
      "name": "Easy Integration",
      "description": "Can be integrated seamlessly with existing React components."
    }
  ],
  "usage": {
    "code": "import { usePortal } from '@custom-react-hooks/all';\n\nconst PortalComponent = () => {\n  const { openPortal, closePortal, isOpen } = usePortal();\n\n  return (\n    <div>\n      <button onClick={openPortal}>Open Portal</button>\n      <button onClick={closePortal}>\n        Close Portal\n      </button>\n      {isOpen && <div className=\"modal\">This is portal content</div>}\n    </div>\n  );\n};\n\nexport default PortalComponent;"
  },
  "apiReference": {
    "returns": [
      {
        "name": "Portal",
        "type": "Component",
        "description": "A component for rendering the portal's children. It only renders its children when the portal is open."
      },
      {
        "name": "openPortal",
        "type": "Function",
        "description": "A function to open the portal."
      },
      {
        "name": "closePortal",
        "type": "Function",
        "description": "A function to close the portal."
      },
      {
        "name": "isOpen",
        "type": "Boolean",
        "description": "A state variable indicating whether the portal is currently open."
      }
    ]
  },
  "useCases": [
    {
      "name": "Modals and Dialogs",
      "description": "Render modals, popups, or dialogs in a DOM node outside of the parent component's hierarchy."
    },
    {
      "name": "Tooltips and Popovers",
      "description": "Create tooltips or popovers that need to break out of their parent's z-index or overflow context."
    },
    {
      "name": "Layered UI Elements",
      "description": "Manage layered UI elements like notifications or full-screen overlays."
    },
    {
      "name": "Dynamic Content Rendering",
      "description": "Render content dynamically in different parts of the document for layout or styling purposes."
    }
  ],
  "contributing": "Contributions to improve `usePortal` are welcome. If you have suggestions or enhancements, feel free to submit issues or pull requests to the repository."
}
