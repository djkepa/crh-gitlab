{
  "name": "useStatus Hook",
  "description": "The `useStatus` hook is designed to monitor the network status of a user's device in React applications. It provides real-time information on whether the user is online or offline and includes additional network details when available.",
  "installation": {
    "currentHook": [
      "npm install @custom-react-hooks/use-status",
      "yarn add @custom-react-hooks/use-status"
    ],
    "allHooks": ["npm install @custom-react-hooks/all", "yarn add @custom-react-hooks/all"]
  },
  "features": [
    {
      "name": "Network Connection Status",
      "description": "Detects and reports the user's online or offline status."
    },
    {
      "name": "Network Information Tracking",
      "description": "When available, provides additional network information such as downlink speed, effective connection type, and round-trip time."
    },
    {
      "name": "Real-Time Updates",
      "description": "Listens to changes in the network status and updates the information accordingly."
    },
    {
      "name": "TypeScript Compatibility",
      "description": "Includes TypeScript definitions to handle non-standard browser APIs like the Network Information API."
    }
  ],
  "usage": {
    "code": "import { useStatus } from '@custom-react-hooks/all';\n\nconst StatusComponent = () => {\n  const { online, downlink, effectiveType, rtt } = useStatus();\n\n  return (\n    <div>\n      <h1>Network Status</h1>\n      <p>{online ? 'Online' : 'Offline'}</p>\n      {downlink && (\n        <p>\n          Downlink Speed:\n          <span>{downlink}Mbps</span>\n        </p>\n      )}\n      {effectiveType && (\n        <p>\n          Effective Type:\n          <span>{effectiveType}</span>\n        </p>\n      )}\n      {rtt && (\n        <p>\n          RTT: <span>{rtt}ms</span>\n        </p>\n      )}\n    </div>\n  );\n};\n\nexport default StatusComponent;"
  },
  "apiReference": {
    "returns": [
      {
        "name": "online",
        "type": "Boolean",
        "description": "Boolean indicating if the user is online."
      },
      {
        "name": "downlink",
        "type": "Number (optional)",
        "description": "The downlink speed in Mbps (optional)."
      },
      {
        "name": "effectiveType",
        "type": "String (optional)",
        "description": "The effective type of the network connection (e.g., '4g', '3g') (optional)."
      },
      {
        "name": "rtt",
        "type": "Number (optional)",
        "description": "The round-trip time in milliseconds (optional)."
      }
    ]
  },
  "useCases": [
    {
      "name": "Online/Offline Indicators",
      "description": "Display indicators showing whether the user is currently online or offline."
    },
    {
      "name": "Adaptive Content Loading",
      "description": "Adjust the amount of data loaded based on network speed (e.g., lower-quality images for slow connections)."
    },
    {
      "name": "Handling Disconnections",
      "description": "Gracefully handle disconnections, e.g., by saving user progress or pausing activities."
    },
    {
      "name": "User Experience Optimization",
      "description": "Optimize user experience based on network conditions, such as simplifying interfaces under poor connectivity."
    }
  ],
  "contributing": "Contributions to improve `useStatus` are welcome. Feel free to submit issues or pull requests to the repository."
}
