{
  "name": "useDragDrop Hook",
  "description": "`useDragDrop` is a hook for enabling drag and drop interactions in your component. It manages the state and events related to dragging and dropping items.",
  "installation": {
    "currentHook": [
      "npm install @custom-react-hooks/use-drag-drop",
      "yarn add @custom-react-hooks/use-drag-drop"
    ],
    "allHooks": ["npm install @custom-react-hooks/all", "yarn add @custom-react-hooks/all"]
  },
  "features": [
    {
      "name": "Combined Drag and Drop Handling",
      "description": "Manages both dragging and dropping within a single hook."
    },
    {
      "name": "Customizable Data Transfer",
      "description": "Allows any data to be associated with the drag operation and retrieved upon dropping."
    },
    {
      "name": "Event Handling",
      "description": "Abstracts away the complexity of drag-and-drop event management."
    },
    {
      "name": "Real-time State Management",
      "description": "Tracks the state of dragging and dropping actions in real-time."
    },
    {
      "name": "SSR Compatibility",
      "description": "Designed to be server-side rendering friendly."
    }
  ],
  "usage": {
    "code": "import React from 'react';\nimport { useDragDrop } from '@custom-react-hooks/all';\n\nconst DraggableItem = ({ id, bindDrag }) => (\n  <div {...bindDrag(id)}>\n    {id}\n  </div>\n);\n\nconst DroppableArea = ({ id, bindDrop, children }) => (\n  <div {...bindDrop(id)}>\n    {children}\n  </div>\n);\n\nconst DragDropComponent = () => {\n  const [itemLocations, setItemLocations] = useState({ Item1: 'outside' });\n\n  const handleDrop = (dragId, dropId) => {\n    setItemLocations((prev) => ({ ...prev, [dragId]: dropId }));\n  };\n\n  const { state, bindDrag, bindDrop } = useDragDrop(handleDrop);\n\n  const renderDraggableItem = (id, location) => {\n    return itemLocations[id] === location ? (\n      <DraggableItem\n        id={id}\n        bindDrag={bindDrag}\n      />\n    ) : null;\n  };\n\n  return (\n    <div>\n      {renderDraggableItem('Item1', 'outside')}\n\n      <div className=\"btns\">\n        <DroppableArea\n          id=\"Area1\"\n          bindDrop={bindDrop}\n          isOver={state.isOver && state.overDropId === 'Area1'}\n        >\n          {renderDraggableItem('Item1', 'Area1')}\n        </DroppableArea>\n\n        <DroppableArea\n          id=\"Area2\"\n          bindDrop={bindDrop}\n          isOver={state.isOver && state.overDropId === 'Area2'}\n        >\n          {renderDraggableItem('Item1', 'Area2')}\n        </DroppableArea>\n      </div>\n    </div>\n  );\n};\n\nexport default DragDropComponent;"
  },
  "apiReference": {
    "parameters": [
      {
        "name": "onDrop",
        "type": "Function",
        "description": "A callback function that is executed when an item is dropped. It receives the `dragId` (id of the dragged item) and `dropId` (id of the drop target) as parameters."
      }
    ],
    "returns": [
      {
        "name": "state",
        "type": "object",
        "description": "An object containing the current drag and drop states.",
        "properties": {
          "isDragging": {
            "type": "Boolean",
            "description": "Indicates whether an item is currently being dragged."
          },
          "isOver": {
            "type": "Boolean",
            "description": "Indicates whether the drag item is currently over a drop target."
          },
          "draggedItemId": {
            "type": "String | null",
            "description": "The id of the currently dragged item."
          },
          "overDropId": {
            "type": "String | null",
            "description": "The id of the drop target over which the drag item is currently located."
          }
        }
      },
      {
        "name": "state.isDragging",
        "type": "Boolean",
        "description": "Indicates whether an item is currently being dragged."
      },
      {
        "name": "state.isOver",
        "type": "Boolean",
        "description": "Indicates whether the drag item is currently over a drop target."
      },
      {
        "name": "state.draggedItemId",
        "type": "String | null",
        "description": "The id of the currently dragged item."
      },
      {
        "name": "state.overDropId",
        "type": "String | null",
        "description": "The id of the drop target over which the drag item is currently located."
      },
      {
        "name": "bindDrag",
        "type": "Function",
        "description": "A function that binds properties for the draggable element. It accepts `dragId` (id of the draggable item) as a parameter."
      },
      {
        "name": "bindDrop",
        "type": "Function",
        "description": "A function that binds properties for the drop target element. It accepts `dropId` (id of the drop target) as a parameter."
      }
    ]
  },
  "useCases": [
    {
      "name": "List and Grid Manipulation",
      "description": "Rearrange items in lists or grids via drag and drop."
    },
    {
      "name": "File Upload Interfaces",
      "description": "Implement drag-and-drop file upload interfaces."
    },
    {
      "name": "Interactive Dashboards",
      "description": "Allow users to customize dashboards by moving widgets or cards."
    },
    {
      "name": "Cross-Component Data Transfer",
      "description": "Facilitate data transfer between different parts of the UI."
    },
    {
      "name": "Visual Editors",
      "description": "Use in visual editors for dragging elements, layers, or tools."
    }
  ],
  "contributing": "Contributions to improve `useDragDrop` are welcome. Please submit issues or pull requests to the repository for any bugs or feature enhancements."
}
